
## API

### 通用
 - 响应格式
    ```
    {
      "code": "string", 编号 success|fail
      "msg": "string", 对应编号的 成功|失败
      "data": //接口业务
    }
    ```

- 部分标准实体返回：
  - 这些参数 顾名思义。其中 id 为该实体的数据库标识，有时也作业业务唯一标识。
    ```
      "modifiedTime": "2023-01-08T02:43:38.640Z",  //更新时间
      "modifiedUserId": "string", //更新人id
      "createdUserId": "string", // 
      "creationTime": "2023-01-08T02:43:38.640Z",
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "deletedUserId": "string",
      "deletedTime": "2023-01-08T02:43:38.640Z",
    ```

- 接口为 非标准 restful （method 遵循）

### 流程设计相关

在流程设计服务里，主要涉及设计和版本的管理。
设计内容则为流程的设计信息，包括节点，连线，人员，条件以及前端其它设计代码管理。
版本则为流程的版本管理，服务的设计为，一个流程可以有多个版本，随着业务的调整可以不断的调整流程，通过版本管理，可以保证现有流程与新流程区分开来，使旧流程不受新流程影响。
- #### 创建流程（设计）***/api/WorkFlow/CreateWorkFlow*** 

  这里的创建很简单，这里实际上是将设计信息的创建统一放到更新操作，一来为了简化新增逻辑，统一在更新处理，二来也是配合前端的交互进行的设计。


  - name：流程设计名称
  - des ：描述

  该操作返回流程基本信息（这时候设计信息还是为空）
  ```
  {
    
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6", //流程id
    "workflowNo": "string",//流程编号
    "name": "string",//流程名称
    "activeVersion": 0, //当前使用版本
    "description": "string" //描述
    ......
  }

  ```
  

  

- #### 流程设计更新 ***/api/WorkFlow/UpdateWorkflow*** 
  该接口用于保存流程设计信息，当流程版本有多个时，每个版本单独存储，每个版本有自己的设计id。

  - **name**：流程设计名称
  - **des**: 描述
  - **drawingInfo**: 前端设计绘制信息，考虑到有些前端流程框架的绘制数据可能比较复杂，为便于回显，可以直接将绘制信息记录起来，需要回显可直接读取

  - **versionDescription**: 版本描述。
  - **workflowId**: 流程唯一标识。 由于一个流程可以有多个版本，所以唯一标识 由 流程id 和 版本确定
    ```
    {
        versionNo: 0 //版本号
        id: //流程设计id 为 CreateWorkFlow  接口返回的 id
    }
    ``` 


  - **workflowNodes**:  流程节点。系统的设计思路为节点负责人员选择，连线负责条件判断。一个节点除了有基本的节点信息，还包括 用户选择器（userSelector 用于在流程流转过程中，解析审批人员）。在这里，节点信息允许括驳回节点指定，即可根据不同的条件判定驳回场景下，应流转到哪个节点（补充一点，在当前系统，不管是拒绝还是驳回，都会构建一条记录信息，而不是“删除”上一步操作，所以每一步都是“下一步”）。

    流程节点数据格式如下：

    ```
    [
        {
          id: 3fa85f64-5717-4562-b3fc-2c963f66afa6  //节点唯一标识，应由前端生成
          name: string //节点名称
          nodeType: 0  //节点类型
          drawingInfo: string //节点绘制信息。预留字段，如果节点的绘制数据较复杂，可以考虑直接保存绘制信息，便于回显
          isWaitingAllUser: true //是否等待所有用户（处理）。
          userSelectors:  //用户选择器，指定通过哪个选择器来解析审批人员
          [
            {
              selectorId: string // 选择器id
              selectorName: string //选择器名称
              selections:  //选项值
              [
                {
                  id: string //选项值id
                  name: string  //选项名称
                }
              ]
              parameter: string //参数，指定参数将作为选择器使用（参数是否有用，具体看选择器的实现，在后台解析审批人员时，会传递该参数）
              description: string //描述
              handleType: 0 //处理类型。分为 只读（类似 抄送，只能看但不能处理）和处理。
            }
          ]
          rejectNodes: //驳回（拒绝）回退的节点配置
          [
            {
              conditions:  //条件
              [
                {
                  conditionId: string //条件id
                  conditionName: string //条件名称
                  parameter: string //参数
                  description: string //  描述
                }
              ]
              nodeId: 3fa85f64-5717-4562-b3fc-2c963f66afa6 //退回的节点id
              nodeName: string // 退回的节点名称
            }
          ]
        }
      ]

    ```

    其中：
    - **nodeType** 节点类型。系统会自动从“开始”节点解析流程。
    其中比较特别的类型是“会签”，使用会签节点的效果与 **isWaitingAllUser** 值为 *true* 的类似，但它是需要连接到它的节点审批通过才会进行下一个节点，它等待的不同来源节点审批完成。有以下枚举类型：
    ```
        /// <summary>
        /// 开始 
        /// </summary>
        Begin,
        /// <summary>
        /// 结束
        /// </summary>
        End,
        /// <summary>
        /// 普通
        /// </summary>
        Judge,
        /// <summary>
        /// 会签，所有指向该节点的节点都要审批完成后到达
        /// </summary>
        Sign
    ```
    - **isWaitingAllUser** : 一个节点可能会分配到多个人审批，通常同一个节点有其中一个人处理后，即可往下一个节点，但是，如果勾选等待所有用户审批，那这个节点就相当会签节点，所有人都通过才会走下一步。

    - **userSelectors** ： 用户选择器集合。用户选择器用于在流转到节点时，解析目标审批人员。用户选择器信息主要包括几个信息：哪个选择器，选了哪个选项，额外参数，人员处理方式。

      - 用户选择器通过 实现 IUserSelector 接口自定义用户选择
          ```
          
          [UserSelector("按用户选择","从所有用户选择")]
          public class UserSelectorB : IUserSelector
          {
              //获取选项值
              public List<Selection> GetSelections()
              {

                  return UserList.Users.Select(u => new Selection { Id = u.Id, Name = u.Name }).ToList();
              }
              //根据选项和参数获取用户（审批人员）
              public List<User> GetUsers(SelectorInput input)
              {
                  var result = new List<User>();
                  switch (input.SelectionId)
                  {
                      default:
                          result.Add(new User { Id = input.SelectionId, Name = UserList.GetUserById(input.SelectionId).Name });
                          break;
                  }
                  return result;
              }
          }
         ```
      - ***/api/WorkFlow/GetAllUserSelectors*** 前端通过该接口获取所有的用户选择器。默认返回的是 实现了 IUserSelector 接口的类型的基本信息，selectorId 为 类型（在文档这里是 UserSelectorB）全称，selectorName 为 UserSelectorAttribute 的第一个参数：
        ``` 
        [UserSelector("按用户选择","从所有用户选择")]
        public class UserSelectorB : IUserSelector

        ``` 
        - 返回选择器集
        ```
        [
          {
            "id": "string", //选择器id（类名全称）
            "name": "string", //选择器名称  UserSelectorAttribute 的第一个参数
            "description": "string" //选择器描述  UserSelectorAttribute 的第二个参数
          }
        ]

        ```


      - ***/api/WorkFlow/GetUserSelectionsOfUserSelector*** 通过该接口根据用户选择器id获取该选择器的选项 。
        - userSelectorId 选择器id。
        - 返回选项集
        ```
        [
          {
            "id": "string", //选项id
            "name": "string" 选项名称
          }
        ]
        ```


    - **rejectNodes** ：拒绝（驳回、回退、回滚）节点配置。通过该配置，根据条件“回到”指定的节点，在必要情况下可以直接退到指定的节点，默认情况下，不配置时，则按原路返回。该配置主要有两项内容：条件，目标节点
      - **nodeId、nodeName** 目标节点id 和名称，
      - **conditions** 条件处理器的配置集合，可配置多个条件，满足其一则跳转到指定节点。条件处理器信息相对简单，主要包含哪个条件处理器以及额外参数。
        - 通过实现 ICondition 接口实现自定义条件处理器。
          ```
            [Condition("条件处理器A")]
            public class ConditionA : ICondition
            {
                public bool CanAccept(ConditionInput input)
                {
                    try
                    {
                        //简单的表达式解析
                        var keyvalue = input.Expression.Split('=');
                        JObject jObject = JObject.Parse(input.WorkTask.FormData);
                        var token = jObject.SelectToken(keyvalue[0]);
                        var value = token.Value<string>();
                        return value.Equals(keyvalue[1]);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
            ``` 
        - ***/api/WorkFlow/GetAllconditions*** 通过该接口可以获取所有的条件处理器信息。
          ```
           [Condition("条件处理器A")]
            public class ConditionA : ICondition

          ```
          - 返回基本的条件处理器信息集
          ```
            [
              {
                "id": "string",  //条件处理器id（类名全称）
                "name": "string", //条件处理器名称  ConditionAttribute 的第一个参数
                "description": "string" //条件处理器描述  ConditionAttribute 的第二个参数
              }
            ]

          ```
  - **workflowLines** : 节点连线信息。该属性记录节点间的连线关系（从哪个节点到哪个节点）以及通行（条件是否满足）条件。
    - **name**: 条件名称。只用于显示
    - **fromNodeId**: 起始节点
    - **toNodeId**: 目标节点
    - **drawingInfo**: 绘制信息。也是冗余备用。万一需要存储前端设计信息，也便于回显
    - **conditions** ：条件选择器信息，用于判断改连线流转是否满足条件，满足才能进行（同 用户选择器 rejectNodes 下的 conditions  ），相当于一个“判断”+连线。



  - ***/api/WorkFlow/UpdateWorkflowActiveVersion*** 用户切换当前激活（应用）的是哪个版本的流程
    - **workflowId** ：流程id
    - **activeVersion** ：激活版本（versionNo）

    - ***/api/WorkFlow/GetAllWorkflowVersions*** 通过该接口根据流程id获取所有的流程和版本信息（一个流程有多个版本）
      - **workflowId** 流程id
      - 返回流程版本集合
        ```
        [
          {
            "workflowId": "3fa85f64-5717-4562-b3fc-2c963f66afa6", //流程id
            "versionNo": 0, //流程版本
            "description": "string", //版本描述
            "modifiedTime": "2023-01-07T13:40:38.791Z",
            "creationTime": "2023-01-07T13:40:38.791Z"
          }
        ]

        ```

- #### 获取所有的流程（设计）基本信息列表 ***​/api​/WorkFlow​/GetAllWorkflows*** 
  - 返回基本信息列表
    ```
      [
        {
          "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6", //流程id
          "workflowNo": "string", //流程编号
          "name": "string", //流程名称
          "activeVersion": 0, //当前应用版本
          "description": "string" //描述
        }
      ]

    ```

- #### 根据流程id和版本号获取详细的流程设计信息 ***/api/WorkFlow/GetWorkflowVersion*** 。
  - **versionNo** 版本号
  - **id** 工作流id

  - 返回详细信息
    ```
    {
      "code": "string",
      "msg": "string",
      "data": {
        "modifiedTime": "2023-01-07T13:48:42.386Z",
        "modifiedUserId": "string",
        "createdUserId": "string",
        "creationTime": "2023-01-07T13:48:42.386Z",
        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "deletedUserId": "string",
        "deletedTime": "2023-01-07T13:48:42.386Z",
        "deleted": true,
        "workflowId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "versionNo": 0,
        "drawingInfo": "string",
        "description": "string",
        "nodeMaps": [], //节点映射。连线和节点的映射信息集合。
        "allNodes": [] //所有节点
      }
    }

    ```
    其中
    - **nodeMaps**  和 allNodes 数据差不多。对于 allNodes  ,类似 接口 ***/api/WorkFlow/UpdateWorkflow***  的参数。 nodeMaps 是 lines 和 nodes 关联的信息的明细，相当于把 连线line 里的 fromNodeId toNodeId 替换为 fromNode toNode ,对应 allNode 里相同nodeId的 node 节点：
      ```
      [
        {
          "mapType": 0,
          "fromNode": {
            "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            "workflowId": {
              "versionNo": 0,
              "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
            },
            "name": "string",
            "nodeType": 0,
            "drawingInfo": "string",
            "isWaitingAllUser": true,
            "userSelectors": [
              {
                "selectorId": "string",
                "selectorName": "string",
                "selections": [
                  {
                    "id": "string",
                    "name": "string"
                  }
                ],
                "parameter": "string",
                "description": "string",
                "handleType": 0
              }
            ],
            "rejectNodes": [
              {
                "conditions": [
                  {
                    "conditionId": "string",
                    "conditionName": "string",
                    "parameter": "string",
                    "description": "string"
                  }
                ],
                "nodeId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                "nodeName": "string"
              }
            ]
          },
          "toNode": {
            "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            "workflowId": {
              "versionNo": 0,
              "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
            },
            "name": "string",
            "nodeType": 0,
            "drawingInfo": "string",
            "isWaitingAllUser": true,
            "userSelectors": [
              {
                "selectorId": "string",
                "selectorName": "string",
                "selections": [
                  {
                    "id": "string",
                    "name": "string"
                  }
                ],
                "parameter": "string",
                "description": "string",
                "handleType": 0
              }
            ],
            "rejectNodes": [
              {
                "conditions": [
                  {
                    "conditionId": "string",
                    "conditionName": "string",
                    "parameter": "string",
                    "description": "string"
                  }
                ],
                "nodeId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                "nodeName": "string"
              }
            ]
          },
          "conditions": [
            {
              "conditionId": "string",
              "conditionName": "string",
              "parameter": "string",
              "description": "string"
            }
          ]
        }
      ]
      ```

  
  



### 流程审批相关

 - #### 新建审批  ***/api/WorkFlow/CreateWorkTask***
    -
      ```
        {
          "workflowId":  
          {
            "versionNo": 0,
            "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
          },
          "name": "string",
          "formData": "string",
          "entityFullName": "string",
          "entityKeyValue": "string",
          "createdUserId": "string"
        }
      ```

    - ***workflowId** ：流程id ：原始流程id + 版本号。通过接口 ***/api/WorkFlow/GetAllWorkflows*** 获取所有的流程设计。返回流程设计基本信息。workflowId.versionNo 对应 GetAllWorkflows 结果集里的 activeVersion。 即这里要创建审批，只需要知道某个流程的以及当前应用的版本（不需要知道所有版本，原则上发起审批的人不需要知道这些）。

    - ***name*** 审批名称（标题）。
    - ***formData*** 表单数据。这个参数是核心。表单数据可以是平台的任意格式，相应的 **userSelector**  和 **condition** 在处理过程也会传递表单数据共解析和判断。
    - ***entityFullName,entityKeyValue*** 外部系统表单类型全称和唯一标识。这两个参数主要是为了便于外部系统自己的分类和查询。在系统表单处需要查询流程信息时，借助这两个数据可以精确查询。
    - ***createdUserId*** 创建用户id。为系统平台的用户id，也可以不传，前提是自定义实现 IWorkflowSession 接口解析当前请求的用户信息。
    - **IWorkflowSession** 的抽象在 **WorkFlowCore.Authorization** 。在**WorkFlowCore.Framework** 实现自定义的 session，并在 **WorkFlowCoreFrameworkService** 替换现有的注册：
        ```
        services.Replace(new ServiceDescriptor(typeof(IWorkflowSession),typeof(DefaultSession), ServiceLifetime.Scoped));
        ```

    - 返回基本流程信息：
      ```
      {
       
        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "deleted": true,
        "workflowId": {
          "versionNo": 0,
          "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
        },
        "name": "string",
        "formData": "string",
        "entityFullName": "string",
        "entityKeyValue": "string",
        "workTaskStatus": 0,
        "isProcessed": true,
        "isPending": true,
        "isProcessing": true,
        "isSimulation": true
      }

      ```
      其中有几个的数据：
      - ***workTaskStatus***  审批状态
        ```
           {
            /// <summary>
            /// 待处理
            /// </summary>
            Pending,
            /// <summary>
            /// 处理中
            /// </summary>
            Processing,
            /// <summary>
            /// 已完成
            /// </summary>
            Processed
        }
        ```

      -  ***isProcessed/isPending/isProcessing*** 为  **workTaskStatus** 的常用判断值

      - ***isSimulation*** 是否是模拟流程。 该属性不在前端传递，而是通过 **创建模拟流程接口** ***/api/WorkFlow/CreateSimulationWorkTask*** 创建的任务。其参数与 **CreateWorkTask** 一致。

 - #### 发起审批  ***/api/WorkFlow/StartWorkTask***
  - 
    ```
    {
      "worktaskId": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    }
    ```
  - ***worktaskId*** 流程审批id。

  - 返回发起审批后的处理待审批记录（每一条记录对应 每一个审批用户）：
      ```
      [
        {
          
          "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "workTaskId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "workStepType": 0,
          "fromNodeId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "fromNodeName": "string",
          "nodeId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "nodeName": "string",
          "handleUser": {
            "id": "string",
            "name": "string"
          },
          "isHandled": true,
          "handleType": 0,
          "comment": "string",
          "resourceIds": "string",
          "isRead": true,
          "readTime": "2023-01-08T02:43:38.627Z",
          "handlerTime": "2023-01-08T02:43:38.627Z",
          "groupId": "string",
          "preStepGroupId": "string",
          "formData": "string",
          "fromForwardStepId": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
        }
      ]

      ```
      其中：
      - ***workStepType***  处理类型。与 流程设计（***/api/WorkFlow/UpdateWorkflow*** ）中，节点 Node 配置中的 handleType对应: 0 //处理类型。分为 只读（类似 抄送，只能看但不能处理）和处理。
        ```
        /// <summary>
        /// 处理（正常需要处理的情况）
        /// </summary>
        Handle,
        /// <summary>
        /// 只读（抄送情况下）
        /// </summary>
        ReadOnly,
        ```

      - ***fromNodeId，fromNodeName*** 来源节点。即是从哪个节点流转到当前的（新发起的节点来源节点为 开始节点）。

      - ***nodeId，nodeName*** 当前审批节点。
      - ***handleUser*** 当前审批人。
      - ***isHandled**  是否已经处理（**workStepType** 为”处理“的情况）
      - ***handleType*** 处理方式：
        ```
        {
            /// <summary>
            /// 通过
            /// </summary>
            Pass,
            /// <summary>
            /// 拒绝
            /// </summary>
            Reject,
            /// <summary>
            /// 撤回
            /// </summary>
            Withdraw,
            /// <summary>
            /// 转发（转给某人审批）
            /// </summary>
            Forward,
            /// <summary>
            /// 未处理（在会签节点其它节点驳回，但是部分未处理的则更新为该状态）
            /// </summary>
            UnWork
        }

        ```
      - ***handlerTime** 处理时间

      - ***comment*** 批文。

       - ***resourceIds*** 附件id。这里设计为只记录附件id。每一次审批都可以根据情况上传附件（附件由平台自己管理附件信息）。通常多个附件英文逗号隔开，但具体什么存和解析，由接入平台定义。
       - ***isRead，readTime*** 是否已读，以及已读时间。（目前这两个状态仅在执行”撤回（***/api/WorkFlow/WithdrawProve***）“操作时，才标记已读，设计考虑在用户查看审批明细时，也应该标记已读）
       - ***groupId*** 分组id。分组id 在业务上没意义，在流程控制上便于”撤回“和”驳回“。*设计上，每一次的节点流转都是一个分组，一个节点可能会根据条件流转到一个或者多个节点上，而这些在同一个节点审批流转的审批就归属为同一组*。
       - ***preStepGroupId*** 前一组id
       - ***formData*** 表单数据。这里审批的表单数据不是发起是的表单数据。下面审批接口（***/api/WorkFlow/PassProve***）会提到。每次审批也都可以有自己的表单数据，当存在这些数据并且 **用户选择器** 和 **条件处理器** 都会应用这些数据进行解析时，也会影响到流程的流转。考虑到的这个场景是应用到每次审批都有可能更新表单的情况（具体在审批过程能否编辑数据，这个得看实现情况）。
       - ***comment*** 批文。
       - ***fromForwardStepId** 来源步骤id。如果是转发才会有值。即标记是哪个审批记录转发过来的。


 - #### 通过审批 ***/api/WorkFlow/PassProve*** 
    - 
      ```
      {
        "comment": "string",
        "stepId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "resourceIds": "string"
      }
      ```

    - ***comment*** 批文
    - ***stepId*** 待审批记录id
    - ***resourceIds*** 附件id（前面提到过）。
    - 返回发起审批后的处理待审批记录（与 发起审批  ***/api/WorkFlow/StartWorkTask*** 接口返回一致）：
 
 - #### 驳回审批 ***/api/WorkFlow/RejectProve*** （参考发起审批  ***/api/WorkFlow/StartWorkTask***）
 - #### 撤回审批 ***/api/WorkFlow/WithdrawProve*** （参考发起审批  ***/api/WorkFlow/ StartWorkTask***）
 
 - #### 转发审批 ***/api/WorkFlow/RejectProve*** 
   转发审批为转发当前审批操作转给另一个人进行审批。与其他审批处理操作相似，但多出一个传参。
 
   ```
     {
       "comment": "string", //批文
       "stepId": "3fa85f64-5717-4562-b3fc-2c963f66afa6", //待审批记录id
       "resourceIds": "string", //附件id
       "userSelectors": 
       [
         {
           "selectorId": "string",
           "selectorName": "string",
           "selections": [
             {
               "id": "string",
               "name": "string"
             }
           ],
           "parameter": "string",
           "description": "string",
           "handleType": 0
         }
       ]
     }
   ```
 
   - ***userSelectors*** 用户选择器。此处转发操作也采用用户选择器进行解析，一来统一规范，二来可扩展性也 高。参考流程设计接口 （流程设计更新 ***/api/WorkFlow/UpdateWorkflow*** ）
 
 
 - #### 获取所有审批步骤 ***/api/WorkFlow/GetAllTaskStepsOfWorkTask*** 
   - ***worktaskId*** 审批id。
   - 返回该审批流所有的审批记录。（与 发起审批  ***/api/WorkFlow/StartWorkTask*** 接口返回一致）。
 
 - #### 获取所有审批步骤（根据entity 信息）  ***/api/WorkFlow/ GetAllTaskStepsOfWorkTaskByEntityInfo***
   - ***entityFullName*** 外部分类全称。
   - ***entityKeyValue*** 外部表单标识。
   - （参考 新建审批  ***/api/WorkFlow/CreateWorkTask*** 里的）
 
 - #### 分页获取用户待处理审批  ***/api/WorkFlow/GetUnHandledWorkTasksOfUser*** 
   - ***CurrentPage*** 当前页（默认从1开始）
   - ***MaxResultCount*** 分页大小
   - 返回流程审批集合.(属性参考 **新建审批**  ***/api/WorkFlow/CreateWorkTask***)
 
 
 - #### 分页获取用户已处理审批  ***/api/WorkFlow/GetHandledWorkTasksOfUser*** 
   参考 **分页获取用户待处理审批**  ***/api/WorkFlow/GetUnHandledWorkTasksOfUser*** 
   - 这里的已处理包括自己发起的，也包括别人发起经由自己审批的。
 
 
 - #### 分页根据entity信息获取处理中的审批  ***​/api​/WorkFlow​/ GetAllProcessingWorkTasksByEntityType*** 
   - ***entityFullName*** 外部分类全称。
   - ***entityKeyValues*** 外部表单标识，多个之间通过英文逗号隔开。
   - 返回流程审批集合.(属性参考 **新建审批**  ***/api/WorkFlow/CreateWorkTask***)
   
 - #### 分页获取用发起的审批  ***/api/WorkFlow/GetWorkTasksOfCreator*** 
   - ***CurrentPage*** 当前页（默认从1开始）
   - ***MaxResultCount*** 分页大小
   - 返回流程审批集合.(属性参考 **新建审批**  ***/api/WorkFlow/CreateWorkTask***)


 - #### 获取审批进度  ***/api/WorkFlow/GetForecastNodeUsers4Task*** 
   - ***taskId*** 审批任务id
   
    - 返回根据表单进行预测的审批节点以及各个节点的审批人
      ```
        [
          {
            "nodeId": "47f79540-d712-41f4-5032-ad4c2fc2d777",
            "nodeName": "开始",
            "users": [
              {
                "handleType": 0,
                "workStepType": 0,
                "id": "5",
                "name": "用户2"
              }
            ],
            "handleUsers": [
              {
                "handleType": 0,
                "workStepType": 0,
                "id": "5",
                "name": "用户2"
              }
            ],
            "readOnlyUsers": [],
            "level": 1,
            "handleType": 0
          }
        ]
      ```
      - ***nodeId*** 节点id
      - ***nodeName*** 节点名称
      - ***handleUsers*** 节点审批人
        - ***id***  处理人id
        - ***name***  处理人名称
        - ***handleType***  处理状态
          - 0 已通过
          - 5 处理中
        - ***workStepType***  处理类型
          - 0 审批
          - 1 抄送

      - ***readOnlyUsers*** 节点抄送人
      - ***users*** 节点审批人（汇总）
      - ***level*** 层级（顺序，从小到大）
      - ***handleType*** 处理状态
        - 0 已通过
        - 5 处理中

- #### 获取审批进度（预测，在发起审批前，POST 请求）  ***/api/WorkFlow/GetForecastNodeUsers4Workflow*** 
    ```
    {
      "formData": "{\"empty\":\"\",\"times\":\"\"}",
      "workflowId": "2e827de5-65bf-491b-9fec-2ec425c4484e",
      "versionNo": 1
    }
    ```
    - ***formData*** 表单数据字符串 
    - ***workflowId*** 流程设计id
    - ***versionNo*** 流程设计版本

  返回值参考 ***/api/WorkFlow/GetForecastNodeUsers4Task*** 
  







 
 - #### 接口现状
  - 目前已实现的接口只能保证基本的流程运作，实际上应用过程可能还有更多的场景没考虑到。由于能力有限，不可能做到面面俱到，特别是对于查询接口，这就需要根据业务自己实现。
