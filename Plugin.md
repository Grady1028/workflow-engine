
## 插件系统
系统实现了一套插件管理逻辑，目的是为了某些插件（扩展点）可以配置和复用。
管理员登录可以对插件进行管理和配置
![Alt text](ReadmeImges/image-7.png)
插件相关流程主要有：**注册、配置、实例化。**
### 注册
#### 注册插件类型
通过 ***PluginManager.RegisterPluginType(Type 插件类型 ,string 插件类型名称)*** 注册插件类型，这一步实际上就是注册抽象类（接口、基类），定义是属于某个类型的插件，比如用户选择器就是一个插件类型。
在插件管理，表现为插件大类：
![Alt text](ReadmeImges/image-8.png)
#### 注册插件实例类型 

在讲注册插件实例之前，先说清楚插件的实现。（***ps:插件系统不支持依赖注入***）
插件的实现，就是接口的实现或者是继承基类的子类。比如实现了用户选择器的接口 IUserSelector 的实现类：DefaultUserSelector、UserSelectorDefaultHttp。
- 可配置插件 。在实现接口或者继承基类的同时，同时实现 ***IConfigurable*** 接口，即可实现插件的可配置。
  ```
     ConfigureContext configureContext;
     public Task Configure(ConfigureContext configureContext)
     {
         this.configureContext = configureContext;
         return Task.CompletedTask;
     }
  ```

通过 ***PluginManager.Register(new ManifestWithConfig(string className, string name,  string description,string entry,string version,Assembly assembly));***
这一步注册插件类型抽象的实现（或者子类），比如用户选择器就有按角色，按用户等方式获取，对应的就是插件类型的实现。其中：
- className 为实现类的全类名，
- name 为插件名称（比如 按用户）
- description 描述
- entry 插件入口，这个参数用于从外部载入插件的dll配置，在代理里配置时，这个参数可以忽略。
- version 版本号（预留，暂时没区分），目前建议直接从类型名称上加版本。
- assembly 插件所在的 程序集
除了手动在系统内注册，也支持注册外部插件（兼容之前的插件，参考下边用户选择器，条件处理器的插件注册），系统会自动根据所注册的插件类型（抽象），遍历插件目录下实现了对应抽象的插件类型，并载入。
- 1 配置外部插件的插件目录，在 ***WorkFlowCoreModule***  ConfigureServices 方法里，配置插件所在的文件夹
  ```
   Configure<PluginOptions>(options =>
   {
       options.BasePath = Path.Combine(AppContext.BaseDirectory, "Plugins");
   });
  ```
- 将实现的插件类库打包放在插件目录下（只要在目录下即可，可以嵌套，会自动遍历）。外部的插件实际上就是实现类生成的类库文件 + 插件描述文件 manifest.json 。插件的目录结构如下：
  - ```
    插件包
    --src
    ----XXXX_Entry.dll  //导出的入口dll（可能会多个）
    --manifest.json  //描述文件
    ```
  - manifest.json  格式如下（已用户选择器为例）
    ```
      {
        "Entry": "UserSelector_PluginDemo.dll", //入口插件格式，插件载入时，将以这个为入口载入（必填）
        "Version": "1.0",
        "ClassName":"插件全类型名称",
        "Name":"中文名称",
        "Description":"类型描述"
      }
    ```
### 配置

对于实现了 ***IConfigurable*** 接口的插件，在插件配置管理则允许实现配置。具体的配置格式要求由插件自定义。平台默认对 key=value 格式自动解析。
![Alt text](ReadmeImges/image-9.png)

每个插件配置有唯一的id（初始化时，自动创建一个配置信息），对于可配置插件，同一个插件实现可以针对应用场景进行多个配置。比如：
![Alt text](ReadmeImges/image-10.png)
![Alt text](ReadmeImges/image-12.png)

### 实例化
插件的实例化只能由插件自行管理，暂时没法使用框架的依赖注入。
实例化支持指定配置id实例化，或者实现某个插件抽象类型的所有实例
```
pluginManager.Resolve<ICondition>(conditionId);
```
```
pluginManager.ResolveAll<IUserSelector>();
```


### 服务注入
（2023-12-16 新增）
对于实现了 ***IServiceResolvable*** 接口的插件，在实例化时，将通过 **InitServiceProvider** 给插件初始化 **IServiceProvider** ，使得插件可以通过容器获取系统注册的服务。诸如 仓储repository 或者其它在模块注册的服务均可以通过服务获取。
```
public class ServiceResolvablePluginDemo : IServiceResolvable
    {

        public void InitServiceProvider(IServiceProvider ServiceProvider)
        {
            //通过 ServiceProvider 可以获取系统services注册的服务
            //var instance = ServiceProvider.GetService<TType>();
        }
    }
```

```
 private IWorkflowRepository workflowRepository;
 public void InitServiceProvider(IServiceProvider ServiceProvider)
 {
     workflowRepository = ServiceProvider.GetService<IWorkflowRepository>();
 }
```


### 实例化
插件的实例化只能由插件自行管理，暂时没法使用框架的依赖注入。
实例化支持指定配置id实例化，或者实现某个插件抽象类型的所有实例
```
pluginManager.Resolve<ICondition>(conditionId);
```
```
pluginManager.ResolveAll<IUserSelector>();
```




 ## 扩展点


 
 ### 用户选择器
  通过自定义实现 IUserSelector 接口实现所需的用户选择器。
  - 实现插件（具体参考 Plugins/UserSelectors/UserSelector_PluginDemo）
    - 新建自定义类库 
    - 引入项目依赖 WorkFlowCore，或者手动引入dll (WorkFlowCore.Common.dll,WorkFlowCore.dll) 
    - 实现自定义逻辑。并创建对应的 manifest.json
      ```
      {
        "Entry": "Condition_PluginDemo.dll", //入口文件dll
        "Version": "1.0" //版本号（目前没应用到，预留）
      }

      ```
    - 编译生成。并将生成的文件按如下结构按如下格式复制到 Host发布目录下 Plugins\UserSelectors
      ![](ReadmeImges/用户选择器插件目录.png )

    - 重启（启动） Host 服务
    - 目前插件的自定义实现类暂时还无法通过ioc 注入参数。除非插件自己维护 ServiceProvider。 


 
  
 ### 条件处理器
  通过自定义实现 ICondition 接口实现所需的用户选择器。
  - 实现插件（具体参考 Plugins/Conditions/UserSelector_PluginDemo）
  - 其他步骤类似用户选择器。只是生成后的插件导入到Host 的路径为  Plugins\Conditions

 
 ### 自定义身份认证
- 自定义实现 ICustomizationVerifyExtension 接口自定义认证扩展。接口返回基本的认证信息。该插件用于自定义验证身份信息，通过扩展可以实现第三方平台的认证。
  ```
   public VerifyOutput Verify(VerifyInput input)

   public class VerifyOutput
    {
        public bool IsValid { get; set; }
        public AuthorizationUser User { get; set; }
        public Claim[] Claims { get; set; }
    }
  ```

  身份认证插件的认证逻辑示意图
  ![Alt text](ReadmeImges/image-13.png)



 ### 自定义流程审批状态同步
- 自定义实现 ICustomTaskStateChangeEventEventHandler 接口自定义流程审批的状态变更通知。每次审批流程状态变化时调用。

![Alt text](ReadmeImges/image-14.png)
