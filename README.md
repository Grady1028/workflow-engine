## 演示地址
***演示环境为dev分支打包***

http://39.101.74.14:8083/index.html

***演示对接业务系统de地址***

http://39.101.74.14:8183/index.html

账号 admin
密码 123qwe

对接演示 git： https://gitee.com/mengtree/workflow-engine-docking

## QQ交流群 ：558038638


## 简介

该流程引擎完全开源免费，致力于打造与平台组织架构无关、高扩展的工作流引擎。
通过自定义用户选择器和条件处理器实现既有业务的组织架构关联和审批过程处理。


## 项目特点
工作流项目有很多，各有特点，而本项目的特点就是解耦。将审批过程中的条件选择与人员派发抽象出来，用户可以根据自身的组织架构特点进行自定义人员与条件的配置，而无需更改逻辑代码。只需要简单的实现条件和人员选择自定义接口即可。

### 传统做法
传统方式，流程审批跟业务系统耦合在一起，特别是审批过程的表单数据判定以及人员（组织）的选择。
![Alt text](ReadmeImges/image.png)

### 本项目的做法
本项目的思想就是将审批过程从业务系统分离。基本思路为：不管是耦合在业务系统的审批还是分离的审批，最终都是更新表单的状态。
![Alt text](ReadmeImges/image-1.png)


## 快速运行

在下载完源码之后，通过几个简单的操作即可运行起项目：
- 在 WorkFlowCoreFrameworkModule 的 PreConfigureServices 方法配置指定启动需要的数据库类型。默认可选 LocalMemory 和 Mysql， LocalMemory 是内存sqlite，无需建库，直接运行体验。
  ```
   public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            WorkFlowCoreFrameworkOptions.DbType = FrameworkConfigOrmType.LocalMemory;
        }
  ```
  
- 如果选择的是 MySql ：
  - 配置数据库字符串链接。如果是Mysql，还需要配置上 版本。建议 8.0 以上。
    在
    ```
        "ConnectionStrings": {
          "Default": "${ConnectionStrings_Default}|HLPexHv+EZ1OHtlrnL2oQLRNTMuY5C1pXgEKuqBqgjaQVWczq9OMn5PErX0cHGUKz7ABrPJZHhI2l4OsnpG7pBt3LAkCvWYfEku7MyTgWjwqVgXq7T6KIMzEdFugrolAFVLC8AChraWFael7QKbJaWUlHEo8mSu1i5wcl+iiFenu8mVYGiIm7+0B0VIhtKbEsBcvvbXhBhrqUsuKXmg9iQ==",
          "DefaultVersion": "8.0.26",
          "SecretPath": "C:\\workflowcore\\secret.config",
          "IsEncrypt": "true",
          "Secret": "${ConnectionStrings_Secret}|"
        }
    ```
    - 通过 ${变量名称}|默认值 的格式配置参数，可以通过配置环境变量，控制台变量启动时指定配置，不指定时，取默认值。这里的连接字符串默认进行了加密处理。*** Mode：CBC  ，Padding：PKCS7 ***
    - IsEncrypt 设为空值 时，可配置明文连接字符串
    - SecretPath 只是用于本地密钥地址。便于本地解密。实际运行时可以通过 指定 ConnectionStrings_Secret 环境变量指定解密密钥。

    - **明文链接字符串配置参考**
     ```
        "ConnectionStrings": {
          "Default": "${ConnectionStrings_Default}|Database=WorkflowCore;Data Source=;Port=3308;UserId=;Password=;Charset=utf8;TreatTinyAsBoolean=false;Allow User Variables=True",
          "DefaultVersion": "8.0.26",
          "SecretPath": "",
          "IsEncrypt": "",
          "Secret": "${ConnectionStrings_Secret}|"
        }
    ```

  - 设置 Host 为启动项。
  - 初始化迁移脚本（如果数据库不是Mysql，默认是Mysql则不需要这一步）。
    -  删除 WorkFlowCore.Framework 下的 Migrations 文件夹。
    -  通过 vs 的 工具=>NuGet包管理器=>程序包管理器控制台 ,默认项目 选择 WorkFlowCore.Framework，输入 add-migration init  命令回车

  - 初始化数据表。通过 vs 的 工具=>NuGet包管理器=>程序包管理器控制台  ,默认项目 选择 WorkFlowCore.Framework，输入 update-database  命令回车 

  - ***注：当前系统使用的 orm 为 ef，如需要自定义实现，则重写 repository ***

## 使用介绍

### 业务模型

- 工作流和业务组织领域的关系
- ![Alt text](ReadmeImges/image-15.png)
- 流程引擎依赖业务系统的业务数据、组织 结构数据。
- ![Alt text](ReadmeImges/image-16.png)


### 源码结构

在介绍使用之前，简单介绍下代码结构。
整个项目包含四个部分：
- 1.WorkFlowCore  是领域核心，所有的业务逻辑在这里面封装交互。在这个部分里，有几个模块：
  -   Authorization 身份认证，里面是获取当前用户信息的抽象接口。因为项目是独立与用户的其它系统，当进行接口（或其它形式）访问时，需要获取当前登录信息时，就需要通过身份接口获取。用户根据实际情况做具体的实现。
  -   EventData 事件消息模型（发布者发布的数据对象）
  -   IRepositories 抽象仓储。在系统设计时，考虑到不同的使用场景下，具体的数据库是不确定的，所以引入仓储，便于扩展。（默认提供了EF 的扩展）
  -   **UserSelectors** 用户选择器。特色模块之一。为用户选择的抽象管理。实际使用时，是需要实现相应的用户选择接口接口扩展用户选择器。后面会详细介绍。
  -   **Conditions** 条件处理器。特色模块之一。通过自定义条件处理器，可以为用户提供丰富的处理选项和逻辑。后面会详细介绍。
  -   Workflows 流程设计模块，后面会介绍。
  -   WorkTasks 流程实例模块，后面会介绍。
  -   Plugins 插件管理模块。
- WorkFlowCore.Framework 领域核心抽象的实现。在这里实现具体用仓储用哪个数据库，有什么用户选择器和条件处理器等待
- WorkFlowCore.Host 接口
- WorkFlowCore.Test 单元测试。

#### 条件处理器和用户选择器插件示意

- 条件处理器
  ![Alt text](ReadmeImges/image-2.png)

- 用户选择器
  ![Alt text](ReadmeImges/image-3.png)


### 流程引擎与业务系统对接示意图
- 业务平台自动登录流程引擎平台

  ![Alt text](ReadmeImges/image-4.png)
- 业务平台调用流程平台接口发起审批

  ![Alt text](ReadmeImges/image-6.png)
- 流程审批状态同步示意图

  ![Alt text](ReadmeImges/image-5.png)

### 使用流程简介

流程引擎单独运行没有什么意义，一般需要结合具体的组织架构和业务进行应用。所以需要根据场景与组织系统进行对接。本系统设计初衷就是为了将引擎核心与业务分离，只需要简单的接口实现既可以跟既有组织架构关联。


- 实现自定义仓储。如果需要使用其它的orm，则需要实现自己的数据库仓储。在实现数据库仓储时，应相应的实现该仓储的工作单元。总之参考默认实现，实现一套自己的仓储。
- 自定义用户选择器。通过实现 IUserSelector 接口实现自己的用户选择器。

    ```
    
    [UserSelector("按用户选择","从所有用户选择")]
    public class UserSelectorB : IUserSelector
    {

        public List<Selection> GetSelections()
        {

            return UserList.Users.Select(u => new Selection { Id = u.Id, Name = u.Name }).ToList();
        }

        public List<User> GetUsers(SelectorInput input)
        {
            var result = new List<User>();
            switch (input.SelectionId)
            {
                default:
                    result.Add(new User { Id = input.SelectionId, Name = UserList.GetUserById(input.SelectionId).Name });
                    break;
            }
            return result;
        }
    }
    ```
   用户选择器说明：
   1. UserSelector 特性。该特性对选择器做指定一个名称和描述，用于在前端显示。如图所示：
   2. 类名全程将作为选择器唯一标识。
   3. GetSelections 方法 指定这个类型的选择器有哪些选项。选择可以是多种多样的。比如作为角色选择器时，选项返回角色列表。
   4. GetUsers 通过将通过选项标识获取实际的用户列表返回。比如角色选择器传入一个角色，将返回这个角色的所有成员。
   
  用户选择器还可以实现 IDefaultUserSelector 以实现”默认选择器“
  ```
  namespace WorkFlowCore.UserSelectors
  {
      public interface IDefaultUserSelector: IUserSelector
      {
          List<string> GetDefaultSelectionIds();
      }
  }

  ```



- 自定义条件处理器。通过实现 ICondition 接口，可自定义条件处理器。条件处理时，将通过工作流表单信息、当前审批人信息、以及条件参数等信息进行判断是否满足某当前处理器所指定的条件。只需要返回是否满足。
  ```
    [Condition("条件处理器A")]
    public class ConditionA : ICondition
    {
        public bool CanAccept(ConditionInput input)
        {
            try
            {
                //简单的表达式解析
                var keyvalue = input.Expression.Split('=');
                JObject jObject = JObject.Parse(input.WorkTask.FormData);
                var token = jObject.SelectToken(keyvalue[0]);
                var value = token.Value<string>();
                return value.Equals(keyvalue[1]);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    ``` 
    条件处理器说：
    1. Condition特性标记。 类似 用户选择器，Condition标记将提供条件名称和描述供前端查看。
    2. CanAccept 只有一个布尔返回值，将通过表达式、当前工作流信息、当前审批步骤 等信息去解析表达式，并得出结果。信息来源广，可以实现解析工作流表单信息，也可以实现通过sql或者其它各种形式的数据判断。同时解析手段也可以根据实际情况扩展，比如解析json、xml或者其它非标准结构的数据，都可以通过自定义实现处理器来 解析判断。

-   创建流程设计。新建一个工作流设计，该流程设计包括各个节点、人员、条件 等的配置信息，主要为一下结构：
    - 节点。审批处理节点，或者说审批步骤。
        - 人员选择器。该节点由哪些人来审批或抄送给谁。
    - 线条。线条用于连接节点。有了线条串起各个节点，才能形成一个通路。
        - 条件处理器。线条配置条件处理器就可以在处理时判断能去到哪些节点。
- 创建工作流实例。设计好流程步骤后。就可以创建相应的审批实例，接着进行审批各项操作。



### 部署方式

当前流程引擎有自己完整的一套工作方式，并不适合进行拆解源码融合到其它系统，最适合的方式就是以分布式的方式进行部署。通过分布式部署的情况下，与业务系统的对接则通过接口进行。

1. 一般情况下，业务系统表单自己维护一个审批状态，业务系统通过调用流程系统接口发起一个审批。也可以自行管理一套表单和状态，只通过本引擎进行流转。
2. 流程系统成功新建一个流程后，将发起一个审批开始的事件，通过订阅该事件进行回调更新业务表单的审批状态。
3. 其它的诸如新建流程设计、流程审批记录等，可通过接口查询，或者自行实现相应的查询接口。


## 快速上手

  [快速上手](/QuickStart.md)

  管理后台快速上手参考

## API

  [API文档](/Api.md)

  常用开放接口基本说明

## 插件系统
  系统实现了一套插件管理逻辑，目的是为了某些插件（扩展点）可以配置和复用。
  管理员登录可以对插件进行管理和配置

  [插件系统文档](/Plugin.md)