#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["WorkFlowCore.Host/WorkFlowCore.Host.csproj", "WorkFlowCore.Host/"]
COPY ["WorkFlowCore.Framework/WorkFlowCore.Framework.csproj", "WorkFlowCore.Framework/"]
COPY ["WorkFlowCore/WorkFlowCore.csproj", "WorkFlowCore/"]
COPY ["WorkFlowCore.Common/WorkFlowCore.Common.csproj", "WorkFlowCore.Common/"]
RUN dotnet restore "WorkFlowCore.Host/WorkFlowCore.Host.csproj"
COPY . .
WORKDIR "/src/WorkFlowCore.Host"
RUN dotnet build "WorkFlowCore.Host.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "WorkFlowCore.Host.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENV ConnectionStrings_Secret empty
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone

ENTRYPOINT ["dotnet", "WorkFlowCore.Host.dll"]