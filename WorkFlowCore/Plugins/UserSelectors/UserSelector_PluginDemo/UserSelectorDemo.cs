﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.UserSelectors;

namespace UserSelector_PluginDemo
{
    [UserSelector("用户选择器插件demo","这是一个插件demo")]
    public class UserSelectorDemo : IUserSelector
    {
        private static  List<Selection> selections = null;
        private static Dictionary<string, List<User>> selectionUsers = null;

        static UserSelectorDemo()
        {
            selections = new List<Selection>();
            selectionUsers = new Dictionary<string, List<User>>();
            for (int i = 0; i < 4; i++)
            {
                selections.Add(new Selection { Id = i.ToString(), Name = $"类型选项{i}" });
                var users = new List<User>();
                for (int j = 0; j < 5; j++)
                {
                    users.Add(new User($"UserSelectorDemo-{i}-{j}", $"选项{i}-用{j}"));
                }
                selectionUsers.Add(i.ToString(), users);
            }
        }

        public List<Selection> GetSelections()
        {
            return selections;
        }

        public List<User> GetUsers(SelectorInput input)
        {
            try
            {
                return selectionUsers[input.SelectionId];
            }
            catch (Exception)
            {
                return new List<User>();
            }
        }
    }
}
