﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.Plugins;

namespace ServiceResolvablePlugin
{
    public class ServiceResolvablePluginDemo : IServiceResolvable
    {

        public void InitServiceProvider(IServiceProvider ServiceProvider)
        {
            //通过 ServiceProvider 可以获取系统services注册的服务
            //var instance = ServiceProvider.GetService<TType>();
        }
    }
}
