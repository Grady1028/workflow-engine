﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Common.Authentication.JWT
{
    public class AuthenticationAuthTokenHeaderParameter : IOperationFilter
    {

        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            operation.Parameters.Add(new OpenApiParameter()
            {
                Name = "access_token",
                In = ParameterLocation.Header,//query header body path formData
                Required = false //是否必选
            });
        }
    }
}
