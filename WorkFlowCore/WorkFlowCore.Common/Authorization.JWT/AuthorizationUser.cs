﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowCore.Common.Authorization.JWT
{
    public class AuthorizationUser
    {
        public AuthorizationUser()
        {
        }

        public AuthorizationUser(string id, string name, bool isManager, string appId)
        {
            Name = name;
            Id = id;
            IsManager = isManager;
            AppId = appId;
        }

        public string Name { get; set;}
        public string Id { get; set; }
        public bool IsManager { get; set; }
        public string AppId { get; set; }
    }
}
