﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowCore.Common.Authorization.JWT
{
    public interface ICustomizationVerify
    {
        VerifyOutput Verify(VerifyInput input);
    }


    public class VerifyInput
    {
        public HttpContext Context { get; set; }
        public string OriginalToken { get; set; }
    }

    public class VerifyOutput
    {
        public bool IsValid { get; set; }
        public AuthorizationUser User { get; set; }
        public Claim[] Claims { get; set; }
    }
}
