﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.Common.Authentication.JWT;
using WorkFlowCore.Common.Authorization.JWT;

namespace WorkFlowCore.Framework.Authorization
{
    public class DefaultCustomizationVerify : ICustomizationVerify
    {

        public VerifyOutput Verify(VerifyInput input)
        {
            return new VerifyOutput
            {
                IsValid = !string.IsNullOrWhiteSpace(input.OriginalToken),
                User = new AuthorizationUser { Id = JWTAuthenticationOptions.DefaultUid, Name = JWTAuthenticationOptions.DefaultUid, IsManager = true }
            };
        }
    }
}
