﻿using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using WorkFlowCore.Authorization;
using WorkFlowCore.Common.Authentication.JWT;

namespace WorkFlowCore.Framework.Authorization
{
    public class DefaultSession : IWorkflowSession
    {
        public DefaultSession()
        {
            _User = new EmptyUser();
        }

        private User _User;

        public User User
        {
            get
            {
                return _User;
            }
        }

        public bool IsManager => _User.IsManager;

        public void SetUser(User user)
        {
            _User = user??new User();
        }
    }
}
