﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus;
using WorkFlowCore.Authorization;
using WorkFlowCore.Common.EventBus;
using WorkFlowCore.EventData;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.EventHandlers
{
    public class AutoHandleStepsEventHandler : ILocalEventHandler<AutoHandleStepsEventData>,
          ITransientDependency
    {
        private readonly WorkTaskManager workTaskManager;
        private readonly IWorkflowSession session;

        public AutoHandleStepsEventHandler(WorkTaskManager workTaskManager, IWorkflowSession session)
        {
            this.workTaskManager = workTaskManager;
            this.session = session;
        }

        public void Handle(AutoHandleStepsEventData data)
        {
            session.SetUser(data.Session.User);
            if (data == null || data.Steps == null) return;
            foreach (var step in data.Steps)
            {
                workTaskManager.PassApprove(step.Id, data.Comment, string.Empty).Wait();
            }
        }

        public Task HandleEventAsync(AutoHandleStepsEventData eventData)
        {
            Handle(eventData); 
            return Task.CompletedTask;
        }
    }
}
