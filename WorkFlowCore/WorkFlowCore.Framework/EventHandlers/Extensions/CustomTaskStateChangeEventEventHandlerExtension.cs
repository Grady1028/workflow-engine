﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.Authorization;
using WorkFlowCore.Common.Authorization.JWT;
using WorkFlowCore.EventData;
using WorkFlowCore.Plugins;

namespace WorkFlowCore.Framework.EventHandlers.Extensions
{
    [PluginDescription("参数：url=[url] 响应地址，要求接受post 请求,接收变信息")]
    [PluginDescription("变更信息数据说明：json 对象 {WorkTask:当前流程实例,OldWorkTaskStatus:旧状态,NewWorkTaskStatus:新状态}，参见 TaskStateChangeEventData 类")]
    public class CustomTaskStateChangeEventEventHandlerExtension : ICustomTaskStateChangeEventEventHandler
    {

        private ConfigureContext configureContext;
        public async Task Configure(ConfigureContext configureContext)
        {
            this.configureContext = configureContext;
        }

        public async Task HandleEventAsync(TaskStateChangeEventData eventData)
        {
            var syncUrl = configureContext.Parameters.GetValueOrDefault("url");
            var httpClient = new HttpClient();

            try
            {
                var value = JsonConvert.SerializeObject(eventData);
                await httpClient.PostAsync(syncUrl, new StringContent(value,Encoding.UTF8, "application/json"));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"CustomTaskStateChangeEventEventHandlerExtension:{ex}");
            }

        }
    }
}
