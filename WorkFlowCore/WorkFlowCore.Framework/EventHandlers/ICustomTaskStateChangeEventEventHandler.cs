﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.EventData;
using WorkFlowCore.Plugins;

namespace WorkFlowCore.Framework.EventHandlers
{
    public interface ICustomTaskStateChangeEventEventHandler : IConfigurable
    {
        Task HandleEventAsync(TaskStateChangeEventData eventData);
    }
}
