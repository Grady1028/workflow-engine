﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus;
using WorkFlowCore.Authorization;
using WorkFlowCore.Common.EventBus;
using WorkFlowCore.EventData;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.EventHandlers
{
    public class SendTaskEventHandler : ILocalEventHandler<SendTaskEventData>, ITransientDependency
    {
        private readonly IWorkflowSession session;


        public SendTaskEventHandler(IWorkflowSession session)
        {
            this.session = session;
        }

        public void Handle(SendTaskEventData data)
        {
            session.SetUser(data.Session.User);
            Console.WriteLine("SendTask");
        }

        public Task HandleEventAsync(SendTaskEventData eventData)
        {
            Handle(eventData);
            return Task.CompletedTask;
        }
    }
}
