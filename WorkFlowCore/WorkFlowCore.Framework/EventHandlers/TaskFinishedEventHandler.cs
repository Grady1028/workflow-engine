﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus;
using WorkFlowCore.Authorization;
using WorkFlowCore.Common.EventBus;
using WorkFlowCore.EventData;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Framework.EventHandlers
{
    public class TaskFinishedEventHandler : ILocalEventHandler<TaskFinishedEventData>,
          ITransientDependency
    {
        private readonly IWorkflowSession session;


        public TaskFinishedEventHandler(IWorkflowSession session)
        {
            this.session = session;
        }

        public void Handle(TaskFinishedEventData data)
        {
            session.SetUser(data.Session.User);
            Console.WriteLine("TaskFinished");
        }

        public Task HandleEventAsync(TaskFinishedEventData eventData)
        {
            Handle(eventData);
            return Task.CompletedTask;
        }
    }
}
