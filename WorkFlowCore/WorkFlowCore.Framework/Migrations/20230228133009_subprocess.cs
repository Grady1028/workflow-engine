﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkFlowCore.Framework.Migrations
{
    public partial class subprocess : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "SubProcessNode_NodeId",
                table: "WorkStepInfos",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<Guid>(
                name: "SubProcessNode_WorkStepId",
                table: "WorkStepInfos",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubProcessNode_NodeId",
                table: "WorkStepInfos");

            migrationBuilder.DropColumn(
                name: "SubProcessNode_WorkStepId",
                table: "WorkStepInfos");
        }
    }
}
