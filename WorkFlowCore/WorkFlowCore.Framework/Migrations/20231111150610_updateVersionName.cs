﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WorkFlowCore.Framework.Migrations
{
    public partial class updateVersionName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkflowId_VersionId",
                table: "WorkTaskInfos",
                newName: "WorkflowId_VersionNo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkflowId_VersionNo",
                table: "WorkTaskInfos",
                newName: "WorkflowId_VersionId");
        }
    }
}
