﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WorkFlowCore.Framework.Migrations
{
    public partial class addMultipleApp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AppId",
                table: "WorkTaskInfos",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "AppId",
                table: "WorkStepInfos",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "AppId",
                table: "WorkflowVersionInfos",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "AppId",
                table: "Workflows",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "AppId",
                table: "FormDesignVersions",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "AppId",
                table: "FormDesignInfos",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AppId",
                table: "WorkTaskInfos");

            migrationBuilder.DropColumn(
                name: "AppId",
                table: "WorkStepInfos");

            migrationBuilder.DropColumn(
                name: "AppId",
                table: "WorkflowVersionInfos");

            migrationBuilder.DropColumn(
                name: "AppId",
                table: "Workflows");

            migrationBuilder.DropColumn(
                name: "AppId",
                table: "FormDesignVersions");

            migrationBuilder.DropColumn(
                name: "AppId",
                table: "FormDesignInfos");
        }
    }
}
