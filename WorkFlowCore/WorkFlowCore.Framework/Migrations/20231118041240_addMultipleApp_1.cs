﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WorkFlowCore.Framework.Migrations
{
    public partial class addMultipleApp_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AppId",
                table: "PluginApplyConfigs",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AppId",
                table: "PluginApplyConfigs");
        }
    }
}
