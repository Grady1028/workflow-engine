﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.MultipleApp;

namespace WorkFlowCore.Framework.MultipleApp
{
    public static class EfQueryExtension
    {
        public static IQueryable<TSource> WhereMultipleApp<TSource>(this IQueryable<TSource> source,string appId)
        {
            if(typeof(TSource).IsAssignableTo<IMultipleApp>())
            {
                if (!string.IsNullOrWhiteSpace(appId))
                {
                    return source.Where(x => ((IMultipleApp)x).AppId == appId);
                }
                else
                {
                    return source.Where(x => ((IMultipleApp)x).AppId == "" || ((IMultipleApp)x).AppId == null);
                }
            }
            return source;
            
        }

        public static void SetMultipleApp<TSource>(this TSource source, string appId)
        {
            if (!string.IsNullOrWhiteSpace(appId) && typeof(TSource).IsAssignableTo<IMultipleApp>())
            {
                if(string.IsNullOrWhiteSpace(((IMultipleApp)source).AppId))
                    ((IMultipleApp)source).AppId = appId;
            }
        }
    }
}
