﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkFlowCore.Framework.UserSelectors
{
    public class UserItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string OrgCode { get; set; }
        public string[] Roles { get; set; }
    }
    public class UserList
    {
        public static List<UserItem> Users { get; private set; }

        static UserList()
        {
            Users = new List<UserItem>()
            {
                new UserItem{Id="1001", Name="张三", OrgCode="1", Roles=new string[]{ "总经理","超级管理员"} },
                new UserItem{Id="1002", Name="李四", OrgCode="1,2", Roles=new string[]{ "财务主管"} },
                new UserItem{Id="1003", Name="王五", OrgCode="1,3", Roles=new string[]{ "项目主管","系统管理员"} },
                new UserItem{Id="1004", Name="赵样", OrgCode="1,3,4", Roles=new string[]{ "产品经理" } },
                new UserItem{Id="1005", Name="钱钱", OrgCode="1,3,5", Roles=new string[]{ "产品经理" } },
                new UserItem{Id="1006", Name="仁兄", OrgCode="1,3,6", Roles=new string[]{ "产品经理" } },
                new UserItem{Id="1007", Name="懿德", OrgCode="1,3,6,7", Roles=new string[]{ "业务员"} },
            };

        }

        public static UserItem GetUserById(string id)
        {
            return Users.FirstOrDefault(u => u.Id == id);
        }

        public static List<UserItem> GetUsersByRole(string role)
        {
            return Users.Where(u => u.Roles.Contains(role)).ToList();
        }

        public static UserItem GetLeaderById(string id)
        {
            var user = Users.FirstOrDefault(u => u.Id == id);
            if (user == null) return null;
            var orgCodes = user.OrgCode.Split(',');
            var leaderOrgCode = string.Join(",", orgCodes.Take(orgCodes.Length - 1));
            var leader = Users.FirstOrDefault(u => u.OrgCode == leaderOrgCode);
            return leader == null ? user : leader;
        }

    }


}
