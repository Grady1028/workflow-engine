﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Framework.UserSelectors
{
    [UserSelector("按用户选择","从所有用户选择")]
    public class UserSelectorB : IUserSelector
    {

        public List<Selection> GetSelections()
        {

            return UserList.Users.Select(u => new Selection { Id = u.Id, Name = u.Name }).ToList();
        }

        public List<User> GetUsers(SelectorInput input)
        {
            var result = new List<User>();
            switch (input.SelectionId)
            {
                default:
                    result.Add(new User { Id = input.SelectionId, Name = UserList.GetUserById(input.SelectionId).Name });
                    break;
            }
            return result;
        }
    }
}
