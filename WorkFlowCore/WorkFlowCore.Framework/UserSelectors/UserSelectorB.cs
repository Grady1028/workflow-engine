﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Framework.UserSelectors
{
    [UserSelector("按角色选择","根据系系统角色解析")]
    public class UserSelectorAllUsers : IUserSelector
    {
        private static List<Selection> selections= new List<Selection>()
            {
                new Selection{ Id="role_systemManager", Name="系统管理员"},
                new Selection{ Id="role_superManager", Name="超级管理员"},
                new Selection{ Id="role_moneyManager", Name="财务主管"},
                new Selection{ Id="role_projectManager", Name="项目主管"},
                new Selection{ Id="role_productManager", Name="产品经理"},
                new Selection{ Id="role_businessMan", Name="业务员"}
            };

        public List<Selection> GetSelections()
        {
            return selections;
        }

        public List<User> GetUsers(SelectorInput input)
        {
            var result = new List<User>();
            switch (input.SelectionId)
            {
                case "role_systemManager":
                    
                case "role_superManager":
                case "role_moneyManager":

                case "role_projectManager":

                case "role_productManager":
                case "role_businessMan":
                    {
                        var users = UserList.GetUsersByRole(selections.First(s=>s.Id==input.SelectionId).Name);
                        result.AddRange(users.Select(u => new User { Id = u.Id, Name = u.Name }));
                        break;
                    }
                default:
                    result.Add(new User { Id = input.SelectionId, Name = input.SelectionId });
                    break;
            }
            return result;
        }
    }
}
