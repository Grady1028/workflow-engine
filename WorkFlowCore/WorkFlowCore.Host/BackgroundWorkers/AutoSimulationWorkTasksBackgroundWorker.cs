﻿using Quartz;
using System.Threading.Tasks;
using Volo.Abp.BackgroundWorkers.Quartz;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.Host.BackgroundWorkers
{
    public class AutoSimulationWorkTasksBackgroundWorker : QuartzBackgroundWorkerBase
    {
        private readonly WorkTaskManager workTaskManager;

        public AutoSimulationWorkTasksBackgroundWorker(WorkTaskManager workTaskManager)
        {
            JobDetail = JobBuilder.Create<AutoClearEmptyWorkflowsBackgroundWorker>().WithIdentity(nameof(AutoClearEmptyWorkflowsBackgroundWorker)).Build();
#if DEBUG

            Trigger = TriggerBuilder.Create().WithIdentity(nameof(AutoClearEmptyWorkflowsBackgroundWorker)).WithCronSchedule("0/10 * * * * ? ").Build();
#else
            Trigger = TriggerBuilder.Create().WithIdentity(nameof(AutoClearEmptyWorkflowsBackgroundWorker)).WithCronSchedule("0 0 0 * * ? ").Build();
#endif
            this.workTaskManager = workTaskManager;
        }

        public override Task Execute(IJobExecutionContext context)
        {
            return workTaskManager.ClearSimulationWorkTasks();
            //return Task.CompletedTask;
        }
    }
}
