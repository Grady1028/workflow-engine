﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using WorkFlowCore.Authorization;
using WorkFlowCore.Common.Authentication.JWT;
using WorkFlowCore.Framework.Migrations;
using WorkFlowCore.Framework.UserSelectors;
using WorkFlowCore.Host.ViewModels.Account;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AccountController : AbpControllerBase
    {
        private readonly JWTAuthenticationManager authenticationManager;
        private readonly IWorkflowSession workflowSession;

        public AccountController(JWTAuthenticationManager authenticationManager, IWorkflowSession workflowSession)
        {
            this.authenticationManager = authenticationManager;
            this.workflowSession = workflowSession;
        }
        [HttpPost("Login")]
        [AllowAnonymous]
        public LoginOutput Login([FromBody] LoginInput input)
        {
            var staticUer = UserList.Users.FirstOrDefault(u => u.Id == input.uid);
            var localPwd = JWTAuthenticationManager.GetLocalPwd();
            if (staticUer != null && input.pwd.Equals(JWTAuthenticationOptions.DefaultPwd))
            {
                var user = new Common.Authorization.JWT.AuthorizationUser { Id = staticUer.Id, Name = staticUer.Name, IsManager = false,AppId = input.appId??"" };
                return new LoginOutput
                {
                    token = authenticationManager.Login(user, null),
                    userInfo = user,
                };
            }
            else
            {

                var inputPwd = JWTAuthenticationManager.PwdEncrypt(input.uid + input.pwd);
                if (!localPwd.Equals(inputPwd))
                {
                    throw new Exception("验证失败");
                }
                var user = new Common.Authorization.JWT.AuthorizationUser { Id = input.uid, Name = input.uid, IsManager = true, AppId = input.appId ?? "" };
                return new LoginOutput
                {
                    token = authenticationManager.Login(user, null),
                    userInfo = user,
                };
            }


        }

        [HttpPost("Logout")]
        public void Logout()
        {
            authenticationManager.Logout(workflowSession.User.Id);
        }

        /// <summary>
        /// 获取所有用户（仅模拟用户）
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllList")]
        [AllowAnonymous]
        public async Task<List<UserSelectors.User>> GetAllList()
        {
            return UserList.Users.Select(u => new UserSelectors.User { Id = u.Id, Name = u.Name }).ToList();
        }

        [HttpPost("AutoLogin")]
        public LoginOutput AutoLogin()
        {
            if (workflowSession?.User == null || string.IsNullOrWhiteSpace(workflowSession?.User.Id))
            {
                throw new Exception("验证失败");
            }
            var user = new Common.Authorization.JWT.AuthorizationUser
            {
                Id = workflowSession?.User?.Id,
                Name = workflowSession?.User?.Name,
                IsManager = workflowSession?.User?.IsManager ?? false,
                AppId = workflowSession?.User?.AppId
            };
            return new LoginOutput
            {
                token = authenticationManager.Login(user, null),
                userInfo = user,
            };
        }

        [HttpGet("GetUserInfo")]
        public async Task<Common.Authorization.JWT.AuthorizationUser> GetUserInfo()
        {
            return new Common.Authorization.JWT.AuthorizationUser()
            {
                Id = workflowSession?.User?.Id,
                Name = workflowSession?.User?.Name,
                IsManager = workflowSession?.User?.IsManager ?? false,
                AppId = workflowSession?.User?.AppId
            };
        }
    }
}
