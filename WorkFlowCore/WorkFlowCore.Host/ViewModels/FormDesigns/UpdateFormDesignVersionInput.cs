﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkFlowCore.FormDesigns;

namespace WorkFlowCore.Host.ViewModels.FormDesigns
{
    public class UpdateFormDesignVersionInput
    {
        public FormDesignId formDesignId { get; set; }
        public string formType{get;set;}
        public string title {get;set;}
        public string description {get;set;}
        public string designContent { get;set; }
        public WorkflowId4FormDesign workflowId { get; set; }
    }
}
