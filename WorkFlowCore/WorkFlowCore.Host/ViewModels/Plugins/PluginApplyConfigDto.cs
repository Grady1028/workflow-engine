﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.Plugins;

namespace WorkFlowCore.Host.ViewModels.Plugins
{
    public class PluginApplyConfigDto
    {
        public PluginsTypeConfig[] Configs { get; set; }
        public List<PluginApplyConfig> ToPluginApplyConfig()
        {
            var pluginApplyConfigs = new List<PluginApplyConfig>();
            foreach (var config in Configs)
            {
                var configItems = config.ConfigItems;
                foreach (var item in configItems)
                {
                    pluginApplyConfigs.Add(new PluginApplyConfig(item.Id, item.Name, config.ExecutableTypeFullName, item.EntryFullName, item.ConfigValue, item.Enabled, item.Order,item.Description));
                }
            }
            return pluginApplyConfigs;
        }

        public static PluginApplyConfigDto FromPluginApplyConfig(List<PluginApplyConfig> pluginApplyConfigs, IReadOnlyList<PluginType> pluginTypes, IEnumerable<ManifestWithConfig> manifestWithConfigs)
        {
            var configs = pluginApplyConfigs.GroupBy(c => new { c.ExecutableTypeFullName });
            var _configs = configs.Select(g => new PluginsTypeConfig
            {
                ExecutableTypeFullName = g.Key.ExecutableTypeFullName,
                Name = pluginTypes.FirstOrDefault(t => t.ExecutableTypeFullName == g.Key.ExecutableTypeFullName)?.Name,
                ConfigItems = g.Select(c =>
                {
                    var manifest = manifestWithConfigs.FirstOrDefault(m => m.ClassName == c.EntryFullName);
                    return new ConfigItem
                    {
                        Configurable = manifest?.Configurable,
                        PluginDescription = manifest?.Description,
                        Description = c.Description,
                        ConfigValue = c.ConfigValue,
                        Enabled = c.Enabled,
                        Id = c.Id,
                        Name = c.Name,
                        Order = c.Order,
                        EntryFullName = c.EntryFullName
                    };
                }).ToArray()
            })
                .ToArray();
            var result = new PluginApplyConfigDto();
            result.Configs = _configs;
            return result;
        }
    }

    public class PluginsTypeConfig
    {
        /// <summary>
        /// 类型名称
        /// </summary>
        public string ExecutableTypeFullName { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// 配置
        /// </summary>
        public ConfigItem[] ConfigItems { get; set; }

    }

    public class ConfigItem
    {
        public long Id { get; set; }
        /// <summary>
        /// 全称
        /// </summary>
        public string EntryFullName { get; set; }
        /// <summary>
        /// 配置名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 配置值
        /// </summary>
        public string ConfigValue { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Order { get; set; }
        public bool? Configurable { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        public string PluginDescription { get; set; }
    }
}
