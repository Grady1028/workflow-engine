﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class CreateWorkTaskInput
    {
        /// <summary>
        /// 流程id
        /// </summary>
        public WorkflowId WorkflowId { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 表单数据（json）
        /// </summary>
        public string FormData { get; set; }
        /// <summary>
        /// 实体全称
        /// </summary>
        public string EntityFullName { get; set; }
        /// <summary>
        /// 实体主键值
        /// </summary>
        public string EntityKeyValue { get; set; }
        /// <summary>
        /// 发起人
        /// </summary>
        public string CreatedUserId { get; set; }
    }
}
