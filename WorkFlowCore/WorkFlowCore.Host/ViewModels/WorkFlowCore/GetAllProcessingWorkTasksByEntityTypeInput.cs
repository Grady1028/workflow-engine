﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class GetAllProcessingWorkTasksByEntityTypeInput
    {
        public string EntityFullName { get; set; }
        public string[] EntityKeyValues { get;set; }
    }
}
