﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    /// <summary>
    /// 获取所有工作流入参
    /// </summary>
    public class GetAllWorkTasksInput
    {
        /// <summary>
        /// 当前页
        /// </summary>
        public int CurrentPage { get; set; } = 1;
        /// <summary>
        /// 分页大小
        /// </summary>
        public int MaxResultCount { get; set; } = -1;
    }

}
