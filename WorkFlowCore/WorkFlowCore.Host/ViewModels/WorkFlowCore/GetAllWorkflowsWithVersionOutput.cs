﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class GetAllWorkflowsWithVersionOutput
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 流程编号
        /// </summary>
        [Required]
        [StringLength(50)]
        public string WorkflowNo { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        /// <summary>
        /// 激活版本
        /// </summary>
        public int ActiveVersion { get; set; }
    }
}
