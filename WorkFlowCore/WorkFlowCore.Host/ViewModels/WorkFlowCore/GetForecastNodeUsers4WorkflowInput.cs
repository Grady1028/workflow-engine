﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class GetForecastNodeUsers4WorkflowInput
    {
        public Guid workflowId { get; set; }
        public int VersionNo { get; set; }
        public string formData { get; set; }
    }
}
