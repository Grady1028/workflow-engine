﻿using System;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class GetRejectNodesInput
    {
        public Guid WorkStepId { get; set; }
    }
}
