﻿namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class GetWorkTaskByEntityInfoInput
    {
        /// <summary>
        /// 实体全称
        /// </summary>
        public string EntityFullName { get; set; }
        /// <summary>
        /// 实体主键值
        /// </summary>
        public string EntityKeyValue { get; set; }
    }
}
