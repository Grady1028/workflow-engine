﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class ProveInput
    {
        ///// <summary>
        ///// 任务id
        ///// </summary>
        //public int WorktaskId { get; set; }
        /// <summary>
        /// 批语
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// 步骤id
        /// </summary>
        public Guid StepId { get; set; }
        /// <summary>
        /// 附件id集合
        /// </summary>
        public string ResourceIds { get; set; }
        /// <summary>
        /// 审批表单，与发起流程的表单是有区别
        /// </summary>
        public string FormData { get; set; }
    }
}
