﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class StartWorkTaskInput
    {
        public Guid WorktaskId { get; set; }
    }
}
