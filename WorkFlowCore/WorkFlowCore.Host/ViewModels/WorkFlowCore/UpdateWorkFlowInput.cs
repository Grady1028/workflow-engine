﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class UpdateWorkFlowInput
    {
        /// <summary>
        /// 流程名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Des { get; set; }
        /// <summary>
        /// 绘制信息
        /// </summary>
        public string DrawingInfo { get; set; }
        /// <summary>
        /// 版本描述
        /// </summary>
        public string VersionDescription { get; set; }
        /// <summary>
        /// 流程id
        /// </summary>
        public WorkflowId WorkflowId { get; set; }

        /// <summary>
        /// 流程节点
        /// </summary>
        public List<WorkflowNodeDto> WorkflowNodes { get; set; }
        /// <summary>
        /// 流程连线
        /// </summary>
        public List<WorkflowLineDto> WorkflowLines { get; set; }

    }
}
