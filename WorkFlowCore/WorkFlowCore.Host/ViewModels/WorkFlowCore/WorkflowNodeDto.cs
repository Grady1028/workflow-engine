﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.Host.ViewModels.WorkFlowCore
{
    public class WorkflowNodeDto
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 节点名称
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// 节点类型
        /// </summary>
        public WorkNodeType NodeType { get; set; }
        /// <summary>
        /// 绘制信息，前端绘制所需信息
        /// </summary>
        public string DrawingInfo { get; set; }
        /// <summary>
        /// 是否等待所有人处理（默认为否）
        /// </summary>
        public bool IsWaitingAllUser { get; set; }
        /// <summary>
        /// 用户选择器
        /// </summary>
        public List<NodeUser> UserSelectors { get; set; }

        /// <summary>
        /// 拒绝节点
        /// </summary>
        public List<RejectNode> RejectNodes { get; set; }
    }
}
