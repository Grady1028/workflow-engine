﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.Conditions;
using WorkFlowCore.Framework;
using WorkFlowCore.Framework.Repositories4EF;
using WorkFlowCore.Plugins;
using WorkFlowCore.UserSelectors;
using WorkFlowCore.IRepositories;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.Test.Plugins
{
    public class Plugin_Test:PluginTestBase
    {

        [Test]
        public async Task LoadPlugins()
        {

            PluginManager.RegisterPluginType(typeof(ICondition), "条件选择器");
            var pluginManager = GetRequiredService<PluginManager>();

            var condition = pluginManager.Resolve<ICondition>("Condition_PluginDemo.ConditionDemo");

        }
    }


}
