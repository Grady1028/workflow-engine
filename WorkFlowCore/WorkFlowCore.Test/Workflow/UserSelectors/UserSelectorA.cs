﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Test.Workflow.UserSelectors
{
    [UserSelector("用户选择器A")]
    public class UserSelectorA : IUserSelector
    {

        public List<Selection> GetSelections()
        {
            return new List<Selection>()
            {
                new Selection{ Id="creator", Name="创建人"},
                new Selection{ Id="leader", Name="上级领导"},
                new Selection{ Id="boss", Name="总经理"}
            };
        }

        public List<User> GetUsers(SelectorInput input)
        {
            var result = new List<User>();
            switch (input.SelectionId)
            {
                case "creator":
                    
                case "leader":

                case "boss":

                default:
                    result.Add(new User { Id = input.SelectionId, Name = input.SelectionId });
                    break;
            }
            return result;
        }
    }
}
