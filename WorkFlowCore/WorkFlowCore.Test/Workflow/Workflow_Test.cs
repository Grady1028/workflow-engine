﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkFlowCore.Common.EventBus;
using WorkFlowCore.Conditions;
using WorkFlowCore.Framework;
using WorkFlowCore.Framework.Conditions;
using WorkFlowCore.Framework.Repositories4EF;
using WorkFlowCore.IRepositories;
using WorkFlowCore.Test.TestBases;
using WorkFlowCore.Test.Workflow.Conditions;
using WorkFlowCore.Test.Workflow.UserSelectors;
using WorkFlowCore.UserSelectors;
using WorkFlowCore.Workflows;
using WorkFlowCore.WorkTasks;
using Xunit;

namespace WorkFlowCore.Test.Workflow
{
    public class Workflow_Test: WorkflowTestBase
    {
        public Workflow_Test()
        {
            var workflowManager = GetRequiredService<WorkflowManager>();
        }

        [Fact]
        public Workflows.Workflow CreateWorkflow(string workflowNo=null)
        {
            var workflowManager = GetRequiredService<WorkflowManager>();

            workflowManager.CreateWorkflow(workflowNo??"wfno", "name", "des").Wait();

            var workflowRepository = GetRequiredService<IBasicRepository<Workflows.Workflow>>();
            var wf = workflowRepository.GetAsync(w => w.WorkflowNo == (workflowNo ?? "wfno")).Result;
            Assert.IsTrue(wf.WorkflowNo.Equals(workflowNo??"wfno"));
            return wf;
        }
        [Test]
        public void UpdateWorkflow()
        {
            var workflowManager = GetRequiredService<WorkflowManager>();

            CreateWorkflow();

            var workflowRepository = GetRequiredService<IBasicRepository<Workflows.Workflow>>();

            var wf = workflowRepository.GetAsync(w => w.WorkflowNo == "wfno").Result;


            WorkflowId workflowId = new WorkflowId(wf.ActiveVersion, wf.Id);
            var workflowNodes = new List<WorkflowNode>();

            var userSelector = new NodeUser(typeof(UserSelectorA).FullName, "UserSelectorA", new List<UserSelection> { new UserSelection { Id = "creator", Name = "创建人" } }, "选择参数", "des", NodeUser.NodeHandleType.Handle);


            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "开始节点", WorkNodeType.Begin, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "分叉节点", WorkNodeType.HandOut, string.Empty, false,
               new List<NodeUser> {
               userSelector
           }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "普通节点", WorkNodeType.Normal, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "普通节点1", WorkNodeType.Normal, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "会签节点", WorkNodeType.Sign, string.Empty, false,
              new List<NodeUser> {
               userSelector
            }, null));


            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "结束节点", WorkNodeType.End, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            var workflowLines = new List<WorkflowLine>();

            workflowLines.Add(new WorkflowLine("开始-分叉", workflowId, workflowNodes[0].Id, workflowNodes[1].Id, new List<LineCondition>
            {
                //new LineCondition("ConditionA","ConditionA","x=1","testdes")
            }));

            workflowLines.Add(new WorkflowLine("分叉-中间", workflowId, workflowNodes[1].Id, workflowNodes[2].Id, new List<LineCondition>
            {
                new LineCondition(typeof(ConditionA).FullName,"ConditionA","x=1","testdes")
            }));
            workflowLines.Add(new WorkflowLine("分叉-中间1", workflowId, workflowNodes[1].Id, workflowNodes[3].Id, new List<LineCondition>
            {
                new LineCondition("ConditionA","ConditionA","x=2","testdes")
            }));

            workflowLines.Add(new WorkflowLine("中间-会签", workflowId, workflowNodes[2].Id, workflowNodes[4].Id, new List<LineCondition>
            {
            }));

            workflowLines.Add(new WorkflowLine("中间1-会签", workflowId, workflowNodes[3].Id, workflowNodes[4].Id, new List<LineCondition>
            {
            }));

            workflowLines.Add(new WorkflowLine("会签-结束", workflowId, workflowNodes[4].Id, workflowNodes[5].Id, new List<LineCondition>
            {
            }));


            workflowManager.UpdateWorkflow(workflowId.Id, "newname", "des", wf.ActiveVersion, string.Empty, string.Empty, workflowLines, workflowNodes).Wait();

            wf = workflowRepository.GetAsync(w => w.WorkflowNo == "wfno").Result;

            Assert.IsTrue(wf.Name.Equals("newname"));

        }

        [Test]
        public void Approve()
        {
            UpdateWorkflow();

            var workflowManager = GetRequiredService<WorkflowManager>();
            var workTaskManager = GetRequiredService<WorkTaskManager>();

            var workflowRepository = GetRequiredService<IBasicRepository<Workflows.Workflow>>();
            var wf = workflowManager.GetWorkflowByNo("wfno").Result;

            WorkflowId workflowId = new WorkflowId(wf.ActiveVersion, wf.Id);
            var worktask = workTaskManager.CreateWorkTask ( workflowId, "测试流程", JsonConvert.SerializeObject(new { x = 2, y = 1 }), "", "","").Result;

            var steps = workTaskManager.WorkTaskStart(worktask.Id).Result;


            List<WorkStep> fxSteps = steps;

            //分叉节点审批
            foreach (var step in steps)
            {
                steps = HandleStep(WorkStepHandleType.Pass, "分叉节点审批通过", workTaskManager, step.Id);
            }

            //分叉节点撤回
            foreach (var step in fxSteps)
            {
                steps = HandleStep(WorkStepHandleType.Withdraw, "分叉节点撤回", workTaskManager, step.Id);
            }

            //分叉节点重新审批
            foreach (var step in steps)
            {
                steps = HandleStep(WorkStepHandleType.Pass, "分叉节点审批重新通过", workTaskManager, step.Id);
            }

            //中间节点1审批
            HandleStep(WorkStepHandleType.Pass, "中间节点1审批", workTaskManager, steps[0].Id);

            //中间节点1撤回
            steps = HandleStep(WorkStepHandleType.Withdraw, "中间节点1撤回", workTaskManager, steps[0].Id);

            //中间节点1审批
            steps = HandleStep(WorkStepHandleType.Pass, "中间节点1重新审批", workTaskManager, steps[0].Id);

            //会签节点审批
            foreach (var step in steps)
            {
                steps = HandleStep(WorkStepHandleType.Pass, "会签节点审批", workTaskManager, step.Id);
            }

        

            //获取所有过程输出
            var historySteps = workTaskManager.GetAllTaskStepsOfWorkTaskAsync(worktask.Id).Result;

            historySteps.ForEach(st =>
            {
                Console.WriteLine($"{st.HandlerTime}:{st.HandleUser.Name}::{st.HandleType}::{st.Comment}");
            });
        }

        private static List<WorkStep> HandleStep(WorkStepHandleType workStepHandleType, string comment, WorkTaskManager workTaskManager, Guid stepId)
        {
            List<WorkStep> worksteps = new List<WorkStep>();
            List<WorkStep> newsteps = null;
            if (workStepHandleType == WorkStepHandleType.Pass)
                newsteps = workTaskManager.PassApprove(stepId, comment).Result.WorkSteps;
            else if (workStepHandleType == WorkStepHandleType.Reject)
                newsteps = workTaskManager.RejectApprove(stepId, comment).Result.WorkSteps;
            else if (workStepHandleType == WorkStepHandleType.Withdraw)
                newsteps = workTaskManager.Withdraw(stepId, comment).Result.WorkSteps;

            //step.Handle(workStepHandleType, comment);

            if (newsteps != null)
                worksteps.AddRange(newsteps);
            return worksteps;
        }

        [Test]
        public void SubProcessWorkflow()
        {
            var workflowManager = GetRequiredService<WorkflowManager>();

            var workTaskManager = GetRequiredService<WorkTaskManager>();


            var workflowRepository = GetRequiredService<IBasicRepository<Workflows.Workflow>>();

            var workflowNo = Guid.NewGuid().ToString();
            CreateWorkflow(workflowNo);
            var wf = workflowManager.GetWorkflowByNo(workflowNo).Result;


            WorkflowId workflowId = new WorkflowId(wf.ActiveVersion, wf.Id);
            var workflowNodes = new List<WorkflowNode>();

            var userSelector = new NodeUser(typeof(UserSelectorA).FullName, "UserSelectorA", new List<UserSelection> { new UserSelection { Id = "creator", Name = "创建人" } }, "选择参数", "des", NodeUser.NodeHandleType.Handle);


            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "开始节点", WorkNodeType.Begin, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "子流程", WorkNodeType.SubProcess, string.Empty, false,
               new List<NodeUser> {
               userSelector
           }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "子流程节点", WorkNodeType.SubNode, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "普通节点1", WorkNodeType.Normal, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            workflowNodes.Add(new WorkflowNode(Guid.NewGuid(), workflowId, "结束节点", WorkNodeType.End, string.Empty, false,
                new List<NodeUser> {
               userSelector
            }, null));

            var workflowLines = new List<WorkflowLine>();

            workflowLines.Add(new WorkflowLine("开始-子流程", workflowId, workflowNodes[0].Id, workflowNodes[1].Id, new List<LineCondition>
            {
            }));

            workflowLines.Add(new WorkflowLine("子流程-普通节点1", workflowId, workflowNodes[1].Id, workflowNodes[3].Id, new List<LineCondition>
            {
            }));
            workflowLines.Add(new WorkflowLine("普通节点1-结束", workflowId, workflowNodes[3].Id, workflowNodes[4].Id, new List<LineCondition>
            {
            }));

            workflowLines.Add(new WorkflowLine("子流程-子流程节点", workflowId, workflowNodes[1].Id, workflowNodes[2].Id, new List<LineCondition>
            {
            }));

            workflowLines.Add(new WorkflowLine("子流程节点-子流程", workflowId, workflowNodes[2].Id, workflowNodes[1].Id, new List<LineCondition>
            {
            }));

            workflowManager.UpdateWorkflow(workflowId.Id, "newname", "des", wf.ActiveVersion, string.Empty, string.Empty, workflowLines, workflowNodes).Wait();

            wf = workflowRepository.GetAsync(w => w.WorkflowNo == "wfno").Result;

            Assert.IsTrue(wf.Name.Equals("newname"));

            //模拟审批

            var worktask = workTaskManager.CreateWorkTask(workflowId, "测试流程", JsonConvert.SerializeObject(new { x = 2, y = 1 }), "", "", "").Result;

            var steps = workTaskManager.WorkTaskStart(worktask.Id).Result;
            List<WorkStep> handlingSteps = null;

            do
            {
                steps = workTaskManager.GetAllTaskStepsOfWorkTaskAsync(worktask.Id).Result;
                handlingSteps = steps.Where(s => !s.IsHandled).ToList();

                foreach (var handlingStep in handlingSteps)
                {
                    HandleStep(WorkStepHandleType.Pass, "审批通过", workTaskManager, handlingStep.Id);
                }
            }
            while (handlingSteps.Any());
            




            //获取所有过程输出
            var historySteps = workTaskManager.GetAllTaskStepsOfWorkTaskAsync(worktask.Id).Result;

            historySteps.ForEach(st =>
            {
                Console.WriteLine($"{st.HandlerTime}:{st.HandleUser.Name}::{st.HandleType}::{st.Comment}");
            });



        }
        [Test]
        public void Condition_Test()
        {
            var condition = new JsonCondition();
            condition.CanAccept(null);
        }
    }
}
