﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.Common.Authorization.JWT;
using WorkFlowCore.Plugins;

namespace WorkFlowCore.Authorization
{
    public interface ICustomizationVerifyExtension: ICustomizationVerify,IConfigurable
    {
    }
}
