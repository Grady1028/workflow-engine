﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Authorization
{
    public interface IWorkflowSession
    {
        public User User {get;}
        public bool IsManager { get; }

        void SetUser(User user);
    }
}
