﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowCore.Authorization
{
    public class User
    {
        public User()
        {
        }


        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsManager { get; set; }
        public string AppId { get; set; }

    }
}
