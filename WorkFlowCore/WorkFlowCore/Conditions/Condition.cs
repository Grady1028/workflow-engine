﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Conditions
{
    public class Condition
    {
        public Condition()
        {
        }

        internal Condition(string id, string name, string description, bool enabled = true)
        {
            Id = id;
            Name = name;
            Description = description;
            Enabled = enabled;
        }
        /// <summary>
        /// 转换器id
        /// </summary>
        public string Id { get; private set; }
        /// <summary>
        /// 转换器名称
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; private set; }
        public bool Enabled { get; set; }
    }
}
