﻿using WorkFlowCore.Common.EventBus;
using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.Authorization;

namespace WorkFlowCore.EventData
{
    public class SendTaskEventData : BaseEventData
    {
        public IWorkflowSession Session { get; set; }
        public WorkTasks.WorkTask WorkTask { get; set; }
        public List<WorkTasks.WorkStep> WorkSteps { get; set; }

    }
}
