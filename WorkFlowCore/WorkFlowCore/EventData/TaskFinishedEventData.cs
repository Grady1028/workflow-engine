﻿using WorkFlowCore.Common.EventBus;
using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.Authorization;

namespace WorkFlowCore.EventData
{
    public class TaskFinishedEventData : BaseEventData
    {
        public IWorkflowSession Session { get; set; }
        public WorkTasks.WorkTask WorkTask { get; set; }
    }
}
