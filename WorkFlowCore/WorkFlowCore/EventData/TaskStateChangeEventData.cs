﻿using WorkFlowCore.Common.EventBus;
using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.WorkTasks;
using WorkFlowCore.Authorization;

namespace WorkFlowCore.EventData
{
    public class TaskStateChangeEventData : BaseEventData
    {
        public IWorkflowSession Session { get; set; }
        public WorkTasks.WorkTask WorkTask { get; set; }
        public WorkTaskStatus OldWorkTaskStatus { get; set; }
        public WorkTaskStatus NewWorkTaskStatus { get; set; }
        public List<WorkTasks.WorkStep> WorkSteps { get; set; }
    }
}
