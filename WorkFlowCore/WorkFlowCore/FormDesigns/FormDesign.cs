﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.IRepositories;

namespace WorkFlowCore.FormDesigns
{
    public class FormDesign:WithBaseInfoEntity
    {
        public FormDesign()
        {
        }

        public FormDesign(string formType, string title, string description, WorkflowId4FormDesign workflowId)
        {
            FormType = formType;
            Title = title;
            Description = description;
            WorkflowId = workflowId;
            ActiveVersion = 1;
        }



        public void SetActiveVersion(int activeVersion)
        {
            ActiveVersion = activeVersion;
        }
        /// <summary>
        /// 如果未设置则设置激活版本
        /// </summary>
        /// <param name="activeVersion"></param>
        public void SetActiveVersionIfEmpty(int activeVersion)
        {
            if (ActiveVersion == null)
                ActiveVersion = activeVersion;
        }

        public void Update(string formType, string title, string description, WorkflowId4FormDesign workflowId)
        {
            FormType = formType;
            //Title = title;
            Description = description;
            WorkflowId = workflowId;
        }

        public string FormType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? ActiveVersion { get; set; }
        public WorkflowId4FormDesign WorkflowId { get; set; }

    }
}
