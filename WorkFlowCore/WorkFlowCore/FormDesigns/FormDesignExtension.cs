﻿using Mapster;
using MapsterMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.Workflows;

namespace WorkFlowCore.FormDesigns
{
    public static class FormDesignExtension
    {
        private static readonly TypeAdapterConfig _config = new TypeAdapterConfig();
        static FormDesignExtension()
        {
            _config.ForType<FormDesignInfo, FormDesign>()
                .Map(dest => dest.WorkflowId, src => new WorkflowId4FormDesign(src.WorkflowId_Id, src.WorkflowId_Version));

            _config.ForType<FormDesign, FormDesignInfo>()
                .Map(dest => dest.WorkflowId_Id, src => src.WorkflowId==null? Guid.Empty: src.WorkflowId.Id)
                .Map(dest => dest.WorkflowId_Version, src => src.WorkflowId == null ? 0: src.WorkflowId.Version);

        }
        public static FormDesign ToFormDesign(this FormDesignInfo formDesignInfo)
        {
            var mapper = new Mapper(_config);
            return mapper.Map<FormDesign>(formDesignInfo);
        }

        public static FormDesignInfo ToFormDesignInfo(this FormDesign formDesign, FormDesignInfo formDesignInfo = null)
        {
            if (formDesignInfo == null)
                formDesignInfo = new FormDesignInfo();


            var mapper = new Mapper(_config);
            return mapper.Map(formDesign, formDesignInfo);
        }
    }
}
