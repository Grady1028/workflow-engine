﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.FormDesigns
{
    public class FormDesignId
    {
        public FormDesignId()
        {
        }

        public FormDesignId(Guid formDesignId, int version)
        {
            Id = formDesignId;
            Version = version;
        }

        public Guid Id { get; set; }
        public int Version { get; set; }
    }
}
