﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.IRepositories;

namespace WorkFlowCore.FormDesigns
{
    public class FormDesignVersion : WithBaseInfoEntity
    {
        public FormDesignVersion()
        {
        }

        public FormDesignVersion(Guid formDesignId, int version, string designContent)
        {
            FormDesignId = formDesignId;
            Version = version;
            DesignContent = designContent;
        }

        public FormDesignId GetFormDesignId()
        {
            return new FormDesignId(FormDesignId, Version);
        }

        public void Update(string designContent)
        {
            DesignContent = designContent;
        }

        internal void Delete()
        {
            Deleted= true;
            DeletedTime= DateTime.Now;
        }

        public Guid FormDesignId { get; set; }
        public int Version { get; set; }
        public string DesignContent { get; set; }

    }
}
