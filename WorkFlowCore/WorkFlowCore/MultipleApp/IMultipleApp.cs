﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowCore.MultipleApp
{
    public interface IMultipleApp
    {
        public string AppId { get; set; }
    }
}
