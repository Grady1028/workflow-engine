﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WorkFlowCore.Plugins
{
    public interface IConfigurable
    {
        Task Configure(ConfigureContext configureContext);
    }

    public class ConfigureContext
    {
        public Dictionary<string,string> Parameters { get; set; }
        public string OriginalParameters { get; set; }
    }
}
