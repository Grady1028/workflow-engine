﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.Common.SimplePluginLoaders;

namespace WorkFlowCore.Plugins
{
    public class ManifestWithConfig : Manifest
    {
        public ManifestWithConfig()
        {
        }

        public ManifestWithConfig(string className, string name,  string description,string entry,string version,Assembly assembly)
        {
            ClassName = className;
            Name = name;
            Description = description;
            Entry = entry;
            Version = version;
            Assembly = assembly;    
        }


        /// <summary>
        /// 类型名称
        /// </summary>
        public string ClassName { get;  set; }
        /// <summary>
        /// 插件名称
        /// </summary>
        public string Name { get;  set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        public bool Configurable { get; set; }
    }
}
