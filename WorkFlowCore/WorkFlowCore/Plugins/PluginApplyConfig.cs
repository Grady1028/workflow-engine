﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;
using WorkFlowCore.MultipleApp;

namespace WorkFlowCore.Plugins
{
    public class PluginApplyConfig:AggregateRoot<long>,IMultipleApp
    {
        public PluginApplyConfig()
        {
        }

        public PluginApplyConfig(long id, string name, string executableTypeFullName, string entryFullName, string configValue, bool enabled, int order, string description)
        {
            Id = id;
            Name = name;
            ExecutableTypeFullName = executableTypeFullName;
            EntryFullName = entryFullName;
            ConfigValue = configValue;
            Enabled = enabled;
            Order = order;
            Description = description;
        }

        public void Update(string name, string executableTypeFullName, string entryFullName, string configValue, bool enabled, int order, string description)
        {
            Name = name;
            ExecutableTypeFullName = executableTypeFullName;
            EntryFullName = entryFullName;
            ConfigValue = configValue;
            Enabled = enabled;
            Order = order;
            Description = description;
        }

        public string PluginId => $"{EntryFullName}:{Id}";

        /// <summary>
        /// 配置名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 类型名称
        /// </summary>
        public string ExecutableTypeFullName { get; set; }
        /// <summary>
        /// 全称
        /// </summary>
        public string EntryFullName { get; set; }
        /// <summary>
        /// 配置值
        /// </summary>
        public string ConfigValue { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Order { get; set; }
        public string Description { get; set; }
        public string AppId { get; set; }
    }
}
