﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowCore.Plugins
{
    [AttributeUsage(AttributeTargets.Class,AllowMultiple =true)]
    public  class PluginDescriptionAttribute: Attribute
    {
        public PluginDescriptionAttribute(string content)
        {
            Content = content;
        }

        public string Content { get; set; }
    }
}
