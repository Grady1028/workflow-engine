﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkFlowCore.WorkTasks;

namespace WorkFlowCore.UserSelectors
{
    /// <summary>
    /// 选择器输入
    /// </summary>
    public class SelectorInput
    {
        /// <summary>
        /// 选择项id
        /// </summary>
        public string SelectionId { get; internal set; }
        /// <summary>
        /// 表达式
        /// </summary>
        public string Expression { get; internal set; }
        /// <summary>
        /// 工作任务
        /// </summary>
        public WorkTask WorkTask { get; internal set; }
        /// <summary>
        /// 当前步骤
        /// </summary>
        public WorkStep CurrentStep { get; internal set; }
    }
}
