﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.UserSelectors
{
    /// <summary>
    /// 用户选择器
    /// </summary>
    public class UserSelector
    {
        public UserSelector()
        {
        }

        internal UserSelector(string id, string name, string description = null, bool enabled = true)
        {
            Id = id;
            Name = name;
            Description = description;
            Enabled = enabled;
        }
        /// <summary>
        /// 选择器id
        /// </summary>
        public string Id { get; private set; }
        /// <summary>
        /// 选择器名称
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; private set; }
        public bool Enabled { get; set; }

    }
}
