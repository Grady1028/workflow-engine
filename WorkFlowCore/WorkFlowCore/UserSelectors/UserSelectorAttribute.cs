﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.UserSelectors
{
    public class UserSelectorAttribute: Attribute
    {
        public string Name;
        public string Description;

        public UserSelectorAttribute(string name, string description = null)
        {
            this.Name = name;
            Description = description;
        }
    }
}
