﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.WorkTasks
{
    public class ForecastNodeUser
    {
        public Guid NodeId { get; set; }
        /// <summary>
        /// 节点名称
        /// </summary>
        public string NodeName { get; set; }

        public List<ForecastUser> Users { get; set; }
        public List<ForecastUser> HandleUsers { get => Users.Where(u => u.WorkStepType == WorkStepType.Handle).ToList(); }
        public List<ForecastUser> ReadOnlyUsers { get => Users.Where(u => u.WorkStepType == WorkStepType.ReadOnly).ToList(); }
        public int Level { get; set; }
        public WorkStepHandleType? HandleType { get=> Users.Any(u=>u.HandleType!=null)==false?null: Users.Where(u => u.HandleType != null).OrderBy(u=>u.HandleType).FirstOrDefault()?.HandleType; }
    }


    public class ForecastUser : User
    {
        public ForecastUser(User user, WorkStepHandleType? HandleType = null, WorkStepType workStepType = default)
        {
            base.Id = user.Id;
            base.Name = user.Name;
            this.HandleType = HandleType;
            WorkStepType = workStepType;
        }

        public WorkStepHandleType? HandleType { get; set; }
        /// <summary>
        /// 阶段类型
        /// </summary>
        public WorkStepType WorkStepType { get; set; }
    }

}
