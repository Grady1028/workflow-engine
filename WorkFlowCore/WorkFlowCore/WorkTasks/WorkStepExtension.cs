﻿using Mapster;
using MapsterMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.WorkTasks
{
    public static class WorkStepExtension
    {

        private static readonly TypeAdapterConfig _config = new TypeAdapterConfig();
        static WorkStepExtension()
        {
            _config.ForType<WorkStep, WorkStepInfo>()
                .Map(dest => dest.HandleUser_Id, src => src.HandleUser.Id)
                .Map(dest => dest.HandleUser_Name, src => src.HandleUser.Name)
                .Map(dest => dest.SubProcessNode_NodeId, src => src.SubProcessNode.NodeId)
                .Map(dest => dest.SubProcessNode_WorkStepId, src => src.SubProcessNode.WorkStepId);

            _config.ForType<WorkStepInfo, WorkStep>()
                .Map(dest => dest.HandleUser, src => new UserSelectors.User(src.HandleUser_Id, src.HandleUser_Name))
                .Map(dest => dest.SubProcessNode, src => new SubProcessNode(src.SubProcessNode_NodeId, src.SubProcessNode_WorkStepId));

        }
        public static WorkStep ToWorkStep(this WorkStepInfo workStepInfo)
        {
            var mapper = new Mapper(_config);
            return mapper.Map<WorkStep>(workStepInfo);
        }

        public static WorkStepInfo ToWorkStepInfo(this WorkStep workStep, WorkStepInfo workStepInfo=null)
        {
            if(workStepInfo==null)
            workStepInfo = new WorkStepInfo();
            var mapper = new Mapper(_config);
            return mapper.Map(workStep, workStepInfo);
        }
    }
}
