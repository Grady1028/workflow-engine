﻿using Confluent.Kafka;
using Mapster;
using MapsterMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.WorkTasks
{
    public static class WorkTaskExtension
    {

        private static readonly TypeAdapterConfig _config = new TypeAdapterConfig();
        static WorkTaskExtension()
        {
            _config.ForType<WorkTask, WorkTaskInfo>()
                .Map(dest => dest.WorkflowId_Id, src => src.WorkflowId.Id)
                .Map(dest => dest.WorkflowId_VersionNo, src => src.WorkflowId.VersionNo);

            _config.ForType<WorkTaskInfo, WorkTask>()
                .Map(dest => dest.WorkflowId, src => new Workflows.WorkflowId(src.WorkflowId_VersionNo, src.WorkflowId_Id));

        }

        public static WorkTask ToWorkTask(this WorkTaskInfo workTaskInfo)
        {
            var mapper = new Mapper(_config);
            return mapper.Map<WorkTask>(workTaskInfo);
        }

        public static WorkTaskInfo ToWorkTaskInfo(this WorkTask workTask, WorkTaskInfo workTaskInfo=null)
        {
            if(workTaskInfo==null)
                workTaskInfo = new WorkTaskInfo();
            var mapper = new Mapper(_config);
            return mapper.Map(workTask, workTaskInfo);
        }
    }
}
