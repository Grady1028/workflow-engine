﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using WorkFlowCore.IRepositories;
using WorkFlowCore.UserSelectors;

namespace WorkFlowCore.Workflows
{
   public class NodeUser
    {
        public NodeUser()
        {
        }

        public NodeUser(string selectorId, string selectorName, List<UserSelection> selections, string parameter, string description, NodeHandleType handleType)
        {
            SelectorId = selectorId;
            SelectorName = selectorName;
            Selections = selections;
            Parameter = parameter;
            Description = description;
            HandleType = handleType;
        }

        /// <summary>
        /// 选择器id
        /// </summary>
        public string SelectorId { get; set; }
        /// <summary>
        /// 选择器名称
        /// </summary>
        public string SelectorName { get; set; }
        /// <summary>
        /// 用户选项
        /// </summary>
        public List<UserSelection> Selections { get; set; }
        /// <summary>
        /// 参数
        /// </summary>
        [StringLength(1000)]
        public string Parameter { get; set; }

        [StringLength(500)]
        public string Description { get; set; }
        public NodeHandleType HandleType { get; set; }

        /// <summary>
        /// 节点处理类型
        /// </summary>
        public enum NodeHandleType
        {
            /// <summary>
            /// 处理
            /// </summary>
            Handle,
            /// <summary>
            /// 只读
            /// </summary>
            Readonly
        }
    }
}
