﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkFlowCore.Workflows
{
    public class UserSelection
    {
        /// <summary>
        /// 选择项id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 选择项名称
        /// </summary>
        public string Name { get; set; }
    }
}
