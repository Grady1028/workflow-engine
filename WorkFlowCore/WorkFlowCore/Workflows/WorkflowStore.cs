﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Services;
using WorkFlowCore.IRepositories;


namespace WorkFlowCore.Workflows
{
    public class WorkflowStore: IDomainService
    {
        private readonly IBasicRepository<WorkflowVersionInfo, Guid> versionRepository;
        public WorkflowStore(IBasicRepository<WorkflowVersionInfo, Guid> versionRepository)
        {
            this.versionRepository = versionRepository;
        }
        public async Task<WorkflowVersionInfo> GetWorkflowVersionInfo(Guid workflowId, int versionNo)
        {
            return await versionRepository.GetAsync(v => v.WorkflowId == workflowId && v.VersionNo == versionNo);
        }
        public async Task<WorkflowVersion> GetWorkflowVersion(Guid workflowId, int versionNo)
        {
            return (await GetWorkflowVersionInfo(workflowId, versionNo)).ToWorkflowVersion();
        }
    }
}
