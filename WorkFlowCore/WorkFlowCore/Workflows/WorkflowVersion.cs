﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using WorkFlowCore.IRepositories;

namespace WorkFlowCore.Workflows
{
    public class WorkflowVersion: WithBaseInfoEntity
    {

        public WorkflowVersion(Guid id,Guid workflowId, int versionNo ,string drawingInfo, string description)
        {
            Id = id;
            WorkflowId = workflowId;
            DrawingInfo = drawingInfo;
            VersionNo = versionNo;
            Description = description;
        }

        public WorkflowVersion()
        {
        }

        public void Update(int versionNo, string drawingInfo, string description)
        {
            VersionNo = versionNo;
            DrawingInfo = drawingInfo;
            Description = description;
        }

        /// <summary>
        /// 流程编号
        /// </summary>
        [Required]
        public Guid WorkflowId { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        [Required]
        public int VersionNo { get; set; }
        /// <summary>
        /// 绘制信息，前端绘制所需信息
        /// </summary>
        public string DrawingInfo { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [StringLength(2000)]
        public string Description { get; set; }
        /// <summary>
        /// 节点映射信息
        /// </summary>
        public List<NodeMap> NodeMaps { get; set; }

        public List<WorkflowNode> AllNodes { get;  set; }
        public bool IsLock { get; set; }

        public void SetNodeMaps(List<NodeMap> nodeMaps)
        {
            NodeMaps = nodeMaps;
            var nodes =NodeMaps.Select(v => v.FromNode).ToList();
            nodes.AddRange(NodeMaps.Select(v => v.ToNode));

            AllNodes= nodes.Distinct().ToList();

        }
    }
}
