import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import workflow from './modules/workflow'
import user from './modules/organisationUser'
import dynamicFormDesign from './modules/dynamicFormDesign'
import plugin from './modules/plugins'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    workflow,
    user,
    dynamicFormDesign,
    plugin,
  },
  getters
})

export default store
