import {
  getToken,
  setToken,
  removeToken,
  getCurrentUser,
  setCurrentUser,
} from "@/utils/auth";
import { resetRouter } from "@/router";
import { MessageBox, Message } from "element-ui";
import request from "@/utils/request";

const getDefaultState = () => {
  return {};
};

const mutations = {};
const state={};
const actions = {
  addOrUpdateConfig({ commit, state }, data) {
    return request({
      url: "/api/Plugin/AddOrUpdateConfig",
      method: "post",
      data: data,
      params: {
        id: data.id,
      },
    });
  },

  getPluginApplyConfigs({ commit, state }, params) {
    return request({
      url: "/api/Plugin/GetPluginApplyConfigs",
      method: "get",
      params: params,
    });
  },
  getManifestWithConfigs({ commit, state }, params) {
    return request({
      url: "/api/Plugin/GetManifestWithConfigs",
      method: "get",
      params: params,
    });
  },
  loadPlugins({ commit, state }, params) {
    return request({
      url: "/api/Plugin/LoadPlugins",
      method: "get",
      params: params,
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
