import { getToken, setToken, removeToken } from "@/utils/auth";
import { resetRouter } from "@/router";
import { MessageBox, Message } from "element-ui";
import request from "@/utils/request";

const getDefaultState = () => {
  return {};
};

const state = getDefaultState();

const mutations = {};

const actions = {
  health({ commit, state }, params) {
    return request({
      url: "/api/Health",
      method: "get",
      params: params,
    });
  },
  getAllconditions({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetAllconditions",
      method: "get",
      params: params,
    });
  },
  getAllUserSelectors({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetAllUserSelectors",
      method: "get",
      params: params,
    });
  },
  getUserSelectionsOfUserSelector({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetUserSelectionsOfUserSelector",
      method: "get",
      params: params,
    });
  },
  createWorkFlow({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/CreateWorkFlow",
      method: "post",
      data: data,
    });
  },
  getAllWorkflows({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetAllWorkflows",
      method: "get",
      params: params,
    });
  },
  getAllWorkflowVersions({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetAllWorkflowVersions",
      method: "get",
      params: params,
    });
  },
  getWorkflowVersion({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetWorkflowVersion",
      method: "get",
      params: params,
    });
  },
  delete({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/delete",
      method: "delete",
      params: params,
    });
  },
  updateWorkflowActiveVersion({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/UpdateWorkflowActiveVersion",
      method: "put",
      data: data,
    });
  },
  updateWorkflow({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/UpdateWorkFlow",
      method: "put",
      data: data,
    });
  },
  createWorkTask({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/CreateWorkTask",
      method: "post",
      data: data,
    });
  },
  createSimulationWorkTask({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/CreateSimulationWorkTask",
      method: "post",
      data: data,
    });
  },
  getWorkTask({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetWorkTask",
      method: "get",
      params: params,
    });
  },
  startWorkTask({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/StartWorkTask",
      method: "post",
      data: data,
    });
  },
  passProve({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/PassProve",
      method: "post",
      data: data,
    });
  },
  rejectProve({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/RejectProve",
      method: "post",
      data: data,
    });
  },
  withdrawProve({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/WithdrawProve",
      method: "post",
      data: data,
    });
  },
  forwardProve({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/ForwardProve",
      method: "post",
      data: data,
    });
  },
  getAllTaskStepsOfWorkTask({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetAllTaskStepsOfWorkTask",
      method: "get",
      params: params,
    });
  },
  clearSimulationRecord({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/ClearSimulationRecord",
      method: "post",
      data: data,
    });
  },
  getUnHandledWorkTasksOfUser({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetUnHandledWorkTasksOfUser",
      method: "get",
      params: params,
    });
  },
  getHandledWorkTasksOfUser({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetHandledWorkTasksOfUser",
      method: "get",
      params: params,
    });
  },
  getUnHandledWorkStepsOfUser({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetUnHandlerWorkStepsOfUser",
      method: "get",
      params: params,
    });
  },
  createAndStartWorkTask({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/createAndStartWorkTask",
      method: "post",
      data: data,
    });
  },
  getAllWorkflowsWithVersion({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetAllWorkflowsWithVersion",
      method: "get",
      params: params,
    });
  },
  getAllTaskStepsOfWorkTaskByEntityInfo({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetAllTaskStepsOfWorkTaskByEntityInfo",
      method: "get",
      params: params,
    });
  },

  getWorkTasksOfCreator({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetWorkTasksOfCreator",
      method: "get",
      params: params,
    });
  },

  getAllWorkTasks({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetAllWorkTasks",
      method: "get",
      params: params,
    });
  },
  deleteWorkTasks({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/deleteWorkTasks",
      method: "delete",
      data: data,
    });
  },
  GetAllUserForSimulation({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetAllUserForSimulation",
      method: "get",
      params: params,
    });
  },

  GetRejectNodes({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetRejectNodes",
      method: "get",
      params: params,
    });
  },
  deleteWorkTask({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/DeleteWorkTask",
      method: "delete",
      params: params,
    });
  },
  updateWorkTaskFormData({ commit, state }, data) {
    return request({
      url: "/api/WorkFlow/UpdateWorkTaskFormData",
      method: "put",
      data: data,
    });
  },

  getUsersOfUserSelecton({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetUsersOfUserSelecton",
      method: "get",
      params: params,
    });
  },
  getForecastNodeUsers4Task({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetForecastNodeUsers4Task",
      method: "get",
      params: params,
    });
  },
  getForecastNodeUsers4Workflow({ commit, state }, params) {
    return request({
      url: "/api/WorkFlow/GetForecastNodeUsers4Workflow",
      method: "post",
      data: params,
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
