import axios from "axios";
import { MessageBox, Message } from "element-ui";
import store from "@/store";
import { getToken, getCurrentUser ,getThirdpartToken} from "@/utils/auth";

// create an axios instance
var axiosconfig = {
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 60000, // request timeout
};
//生成环境自定义配置
if (globalconfig_baseURL && globalconfig_baseURL.length > 0) {
  axiosconfig.baseURL = globalconfig_baseURL;
}
const service = axios.create(axiosconfig);

// request interceptor
service.interceptors.request.use(
  (config) => {
    // do something before request is sent
    if (getToken()) {
      // let each request carry token
      // ['Authorization'] is a custom headers key
      // please modify it according to the actual situation
      config.headers["Authorization"] = "Bearer " + getToken();
      //获取当前用户信息
      config.headers["user-info"] = encodeURI(
        JSON.stringify(getCurrentUser() || {})
      );
    }
    if(getThirdpartToken())
    {
      config.headers["ThirdpartAuthorization"] = getThirdpartToken();
    }
    return config;
  },
  (error) => {
    // do something with request error
    console.log(error); // for debug
    debugger;
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  (response) => {
    const res = response.data;
    if (res.code == "error") {
      return Promise.reject(new Error(res.msg || "Error"));
    } else {
      if (res && res.code == "success") return res.data;
      else {
        Message({
          message: res.msg,
          type: "error",
          duration: 5 * 1000,
        });
        return Promise.reject(new Error(res.msg || "Error"));
      }

      return res.data;
    }
  },
  (error) => {
    if (error.response.status == 401) {
      store.dispatch("user/resetToken").then(() => {
        if(location.href.indexOf('login')) return;
        location.reload();
      });
    }
    Message({
      message: error.message,
      type: "error",
      duration: 5 * 1000,
    });
    return Promise.reject(error);
  }
);

export default service;
